@extends('admin.layouts.error')
@section('title', $exception->getStatusCode())
@section('content')
<div class="error_page_header">
    <div class="uk-width-8-10 uk-container-center">
        Error {{$exception->getStatusCode()}}
    </div>
</div>
<div class="error_page_content">
    <div class="uk-width-8-10 uk-container-center">
        <p class="heading_b">Page not found</p>
        <p class="uk-text-large">
            The requested URL <span class="uk-text-muted">{{url()->current()}}</span> was not found on this server.
        </p>
        <a href="#" onclick="history.go(-1);return false;">Go back to previous page</a>
    </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/admin/css/error_page.min.css')}}">
@endsection
