<!doctype html>
<html lang="en">
<head>
  <title>@yield('title') - Tab Sewa</title>
  @include('admin.includes.meta')
  @stack('b-css')
  <style>
  .content-preloader-overlay {
    background-color: black;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
    background-color: rgba(0, 0, 0, 0.4);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: none;
    z-index: 10000;
  }
  </style>
</head>
<body class="login_page app_theme_f">
  @yield('content')
  @include('admin.includes.jsscript')
  @stack('b-script')
</body>
</html>
