<!doctype html>
<html lang="en">
<head>
  <title>@yield('title') | Tab Sewa</title>
  @stack('a-css')
  @include('admin.includes.meta')
  @stack('b-css')
  <style>
  #top_bar {
    overflow-x: auto;
    overflow-y: hidden;
    height: auto;
  }
  #breadcrumbs {
    overflow: hidden;
    display: inline-block;
    min-width: 100%;
    padding-right: 10%;
  }
  .content-preloader-overlay {
    background-color: black;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
    background-color: rgba(0, 0, 0, 0.4);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: none;
    z-index: 10000;
  }
  </style>
</head>

<body class="sidebar_main_open sidebar_main_swipe app_theme_f">
    @include('admin.includes.header')
    @include('admin.includes.sidebar')
    <div id="page_content">
        @yield('bread','')
        @yield('content')
    </div>
    @stack('a-script')
    @include('admin.includes.jsscript')
    @stack('b-script')
</body>
</html>
