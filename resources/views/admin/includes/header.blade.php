<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                <span class="sSwitchIcon"></span>
            </a>
            <div class="uk-navbar-flip">
                  <ul class="uk-navbar-nav user_actions">
                      <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                      <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                          <a href="#" class="user_action_image">
                            <i class="material-icons md-24 md-light">&#xE853;</i>
                          </a>
                          <div class="uk-dropdown uk-dropdown-small">
                              <ul class="uk-nav js-uk-prevent">
                                  <li><a href="{{url('logout')}}">Logout</a></li>
                              </ul>
                          </div>
                      </li>
                  </ul>
            </div>
        </nav>
    </div>
    <div class="header_main_search_form">
        <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
        <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
            <input type="text" class="header_main_search_input" />
            <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>

        </form>
    </div>
</header>
