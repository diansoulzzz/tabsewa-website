@extends('admin.layouts.default')
@section('title', 'CMS Content')
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  @if ($errors->any())
    <div class="uk-alert uk-alert-danger" data-uk-alert>
      <a href="" class="uk-alert-close uk-close"></a>
        <ul>
        <strong>There was error when change your information</strong>
          @foreach ($errors->all() as $message)
              <li>{{$message}}</li>
          @endforeach
        </ul>
    </div>
  @endif
  <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
    {{ csrf_field() }}
    @if(Auth::guard('web_vendor')->check())
    <input type="hidden" name="vendor" value=1 />
    @endif
      <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-large-10-10">
              <div class="md-card">
                  <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                      <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail">
                            @if(Auth::guard('web_admin')->check())
                              @empty(Auth::guard('web_admin')->user()->url_photo)
                                <img src="{{asset('assets/back/img/avatars/user.png')}}" alt="user avatar"/>
                              @endempty
                              @isset(Auth::guard('web_admin')->user()->url_photo)
                                <img src="{{asset(Storage::disk('public')->url(Auth::guard('web_admin')->user()->url_photo))}}" alt="user avatar"/>
                              @endisset
                            @else
                              @empty(Auth::guard('web_vendor')->user()->url_photo)
                                <img src="{{asset('assets/back/img/avatars/user.png')}}" alt="user avatar"/>
                              @endempty
                              @isset(Auth::guard('web_vendor')->user()->url_photo)
                                <img src="{{asset(Storage::disk('public')->url(Auth::guard('web_vendor')->user()->url_photo))}}" alt="user avatar"/>
                              @endisset
                            @endif
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail"></div>
                          <div class="user_avatar_controls">
                              <span class="btn-file">
                                  <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                  <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                  <input type="file" name="url_photo" id="url_photo">
                              </span>
                              <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i class="material-icons">&#xE5CD;</i></a>
                          </div>
                      </div>
                      <div class="user_heading_content">
                        <h2 class="heading_b">
                          <span class="uk-text-truncate" id="user_edit_uname">
                            @if(Auth::guard('web_admin')->check())
                              {{Auth::guard('web_admin')->user()->nama}}
                            @else
                              {{Auth::guard('web_vendor')->user()->nama}}
                            @endif
                          </span>
                          <span class="sub-heading" id="user_edit_position">
                            @if(Auth::guard('web_admin')->check())
                              {{Auth::guard('web_admin')->user()->departements->first()->nama}}
                            @else
                              {{Auth::guard('web_vendor')->user()->departements->first()->nama}}
                            @endif
                          </span>
                        </h2>
                      </div>
                      <div class="md-fab-wrapper">
                        <button class="md-fab md-fab-primary md-fab-accent" href="#" id="product_edit_submit">
                            <i class="material-icons">&#xE161;</i>
                        </button>
                      </div>
                  </div>
                  <div class="user_content">
                      <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                          <li class="uk-active"><a href="#">Basic</a></li>
                          <li><a href="#">Security</a></li>
                      </ul>
                      <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                          <li>
                              <div class="uk-margin-top">
                                <h3 class="full_width_in_card heading_c">
                                    General Info
                                </h3>
                                <div class="uk-grid" data-uk-grid-margin>
                                  <div class="uk-width-medium-1-1">
                                      <label for="user_edit_uname_control">Nama</label>
                                      @if(Auth::guard('web_admin')->check())
                                        <input class="md-input" type="text" name="nama" required value="{{Auth::guard('web_admin')->user()->nama}}"/>
                                      @else
                                        <input class="md-input" type="text" name="nama" required value="{{Auth::guard('web_vendor')->user()->nama}}"/>
                                      @endif

                                      @foreach ($errors->get('nama') as $message)
                                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                                          <a href="" class="uk-alert-close uk-close"></a>
                                          <p><strong>Error!</strong> {{$message}}</p>
                                        </div>
                                      @endforeach
                                  </div>
                                </div>
                                <h3 class="full_width_in_card heading_c">
                                    Contact info
                                </h3>
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                            <div>
                                                <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </span>
                                                    <label>Email</label>
                                                    @if(Auth::guard('web_admin')->check())
                                                      <input type="text" class="md-input" name="email" value="{{Auth::guard('web_admin')->user()->email}}" disabled/>
                                                    @else
                                                      <input type="text" class="md-input" name="email" value="{{Auth::guard('web_vendor')->user()->email}}" disabled/>
                                                    @endif
                                                </div>
                                            </div>
                                            <div>
                                                <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </span>
                                                    <label>Phone Number</label>
                                                    @if(Auth::guard('web_admin')->check())
                                                      <input class="md-input masked_input" data-parsley-minlength="10" data-inputmask="'mask': '9', 'repeat': 14, 'greedy' : false" type="tel" name="notelp" required value="{{Auth::guard('web_admin')->user()->notelp}}"/>
                                                    @else
                                                      <input class="md-input masked_input" data-parsley-minlength="10" data-inputmask="'mask': '9', 'repeat': 14, 'greedy' : false" type="tel" name="notelp" required value="{{Auth::guard('web_vendor')->user()->notelp}}"/>
                                                    @endif
                                                    @foreach ($errors->get('notelp') as $message)
                                                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                                                        <a href="" class="uk-alert-close uk-close"></a>
                                                        <p><strong>Error!</strong> {{$message}}</p>
                                                      </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </li>
                          <li>

                            <h3 class="full_width_in_card heading_c">
                                Change Password
                            </h3>
                            <div class="uk-grid" data-uk-grid-margin>
                              <div class="uk-width-medium-1-1">
                                  <label for="user_edit_uname_control">Old Password</label>
                                  <input class="md-input" type="password" name="old_password" data-parsley-minlength="6"/>
                                  @foreach ($errors->get('old_password') as $message)
                                    <div class="uk-alert uk-alert-danger" data-uk-alert>
                                      <a href="" class="uk-alert-close uk-close"></a>
                                      <p><strong>Error!</strong> {{$message}}</p>
                                    </div>
                                  @endforeach
                              </div>
                              <div class="uk-width-medium-1-1">
                                  <label for="user_edit_uname_control">Password</label>
                                  <input class="md-input" type="password" id="password" name="password" data-parsley-minlength="6"/>
                                  @foreach ($errors->get('password') as $message)
                                    <div class="uk-alert uk-alert-danger" data-uk-alert>
                                      <a href="" class="uk-alert-close uk-close"></a>
                                      <p><strong>Error!</strong> {{$message}}</p>
                                    </div>
                                  @endforeach
                              </div>
                              <div class="uk-width-medium-1-1">
                                  <label for="user_edit_uname_control">Confirm Password</label>
                                  <input class="md-input" type="password" name="password_confirmation" data-parsley-equalto="#password" data-parsley-minlength="6"  data-parsley-trigger="change"/>
                                  @foreach ($errors->get('password_confirmation') as $message)
                                    <div class="uk-alert uk-alert-danger" data-uk-alert>
                                      <a href="" class="uk-alert-close uk-close"></a>
                                      <p><strong>Error!</strong> {{$message}}</p>
                                    </div>
                                  @endforeach
                              </div>
                            </div>
                          </div>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </form>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script src="{{asset('assets/back/js/custom/uikit_fileinput.min.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/handlebars/handlebars.min.js')}}"></script>
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
    thisform.s_init();
    thisform.i_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $maskedInput = $(".dropify"),
    $maskedInput.length && $maskedInput.dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    });
  },

};
</script>
@endsection
