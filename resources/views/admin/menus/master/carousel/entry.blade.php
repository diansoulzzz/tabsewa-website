@extends('admin.layouts.default')
@section('title', 'Carousel')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="#">Carousel</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Input</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              Input Carousel
          </h3>
      </div>
      <div class="md-card-content large-padding">
          <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="nama">Nama Carousel<span class="req">*</span></label>
                      <input type="text" name="nama" id="nama" required class="md-input" data-parsley-minlength="3" value="{{ old('nama', isset($carousel->nama) ? $carousel->nama : '' ) }}" />
                      @foreach ($errors->get('nama') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="keterangan">Keterangan<span class="req">*</span></label>
                      <textarea name="keterangan" id="keterangan" required class="md-input" rows="4" data-parsley-minlength="3">{{ old('keterangan', isset($carousel->keterangan) ? $carousel->keterangan : '' ) }}</textarea>
                      @foreach ($errors->get('keterangan') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="input-file-b">Photo<span class="req">*</span></label>
                      <input type="file" id="input-file-b" name="foto_url" class="dropify" data-default-file="{{isset($carousel->foto_url) ? $carousel->foto_url : ''}}" />
                      @foreach ($errors->get('foto_url') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
                </div>
            </div>
            @isset($carousel)
              <input type="hidden" name="id" value="{{$carousel->id}}"/>
            @endisset
          </form>
      </div>
  </div>
</div>
@endsection
@push('b-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/admin/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/admin/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endpush
@push('b-script')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/admin/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/admin/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
    thisform.i_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  i_init: function()
  {
    $(".dropify").dropify();
  },
};
</script>
@endpush
