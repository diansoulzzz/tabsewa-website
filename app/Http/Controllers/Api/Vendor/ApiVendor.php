<?php
namespace App\Http\Controllers\Api\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use App\Models\BarangDetail;
use App\Models\DSewa;
use App\Models\BarangFoto;
use App\Models\BarangHargaSewa;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\HSewa;

class ApiVendor extends Controller
{
  public function postOpenToko(Request $request)
  {
    $input = $request->all();
    try
    {
      $user = User::findOrFail(Auth::id());
      $user->is_vendor = 1;
      $user->save();
      // $data = $user->makeVisible('api_token');
      $data = $user;
      return response()->json($this->setSuccessResponse($data,$input));
    }
    catch(ModelNotFoundException $e)
    {
      $error = [];
      return response()->json($this->setErrorResponse($error,$input,'502',$e->getMessage()));
    }
  }

  public function getTransactionDataFromId(Request $request){
    $input = $request->all();
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_penyewa_id',Auth::id())
    ->where('status_transaksi','!=','0')
    ->where('status_transaksi','!=','1')
    ->where('status_transaksi','!=','-1')
    ->find($request->input('id'));
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getTransactionList(Request $request){
    $input = $request->all();
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_penyewa_id',Auth::id())
    ->where('status_transaksi','!=','0')
    ->where('status_transaksi','!=','1')
    ->where('status_transaksi','!=','-1')
    ->orderBy('created_at','desc')
    ->get();
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function postBarangEntry(Request $request){
    $input = $request->all();
    $request->merge($request->input('barang'));

    $barang = new Barang();
    if ($request->has('id')){
      $barang = Barang::find($request->input('id'));
    }
    DB::transaction(function () use($request, $barang) {
      $edit = false;
      if ($request->has('id')){
        $edit = true;
      }
      $barang->nama = $request->input('nama');
      $barang->deposit_min = $request->input('deposit_min');
      $barang->deskripsi = $request->input('deskripsi');
      $barang->barang_sub_kategori_id = $request->input('barang_sub_kategori_id');
      $barang->users_id = Auth::id();
      if (!$edit){
        $barang->stok = $request->input('stok');
        $barang->stok_available = $barang->stok;
        $barang->stok_onbook = 0;
      }
      $barang->save();
      
      $barang_fotos = [];
      foreach ($request->input('barang_fotos') as $key => $foto) {
        $barang_foto = new BarangFoto([
          'foto_url' => $foto['foto_url'],
          'utama' => $foto['utama'],
        ]);
        if (array_key_exists('id', $foto)) {
          $barang_foto->id = $foto['id'];
        };
        $barang_fotos[] = $barang_foto;
      }
      if ($edit){
        $barang->barang_fotos()->forceDelete();
      }
      $barang->barang_fotos()->saveMany($barang_fotos);

      $barang_harga_sewas = [];
      foreach ($request->input('barang_harga_sewas') as $key => $harga_sewa) {
        $barang_harga_sewa = new BarangHargaSewa([
          'harga' => $harga_sewa['harga'],
          'lama_hari' => $harga_sewa['lama_hari'],
        ]);
        if (array_key_exists('id', $harga_sewa)) {
          $barang_harga_sewa->id = $harga_sewa['id'];
        };
        $barang_harga_sewas[] = $barang_harga_sewa;
      }
      if ($edit){
        $barang->barang_harga_sewas()->forceDelete();
      }
      $barang->barang_harga_sewas()->saveMany($barang_harga_sewas);

      if (!$edit){
        $barang_details = [];
        for ($i=0; $i < $request->input('stok'); $i++) {
          $barang_details[] = new BarangDetail([
            'barang_id' => $barang->id,
            'status_barang' => 1,
          ]);
        }
        $barang->barang_details()->saveMany($barang_details);
      }

    }, 5);
    $barang = Barang::with(['user','barang_fotos','barang_harga_sewas','barang_harga_sewa_utama','barang_foto_utama','barang_sub_kategori.barang_kategori','wishlisted'])->find($barang->id);
    $data = $barang;
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function postBarangDelete(Request $request)
  {
    Barang::find($request->input('id'))->delete();
    return $this->getDaftarBarangList($request);
  }

  public function postTransactionChange(Request $request){
    $input = $request->all();
    $request->merge($request->input('h_sewa'));
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewas','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_penyewa_id',Auth::id())
    ->find($request->input('id'));

    $hsewa->status_transaksi = $request->input('status_transaksi');
    $status_name = '';
    if ($hsewa->status_transaksi==-2) {
      $status_name = 'telah ditolak Vendor';
      $hsewa->tgl_konfirmasi_penyewa = Carbon::now();

    } else if ($hsewa->status_transaksi==3) {
      $status_name = 'sudah dikonfirmasi dan sedang diproses oleh Vendor';
      $hsewa->tgl_konfirmasi_penyewa = Carbon::now();

    } else if ($hsewa->status_transaksi==4) {
      $status_name = 'sedang dikirim oleh Vendor';
      $hsewa->tgl_dikirim_penyewa = Carbon::now();

    } else if ($hsewa->status_transaksi==7) {
      $status_name = 'sudah selesai, berikan ulasan ke vendor';
      // KEMBALI STOK
      $dsewas = DSewa::where('h_sewa_id',$hsewa->id)->get();
      foreach ($dsewas as $key => $dsewa) {
        BarangDetail::where('id',$dsewa->barang_detail_id)->update(['status_barang'=>1]);
      }
      $total_stok = $dsewas->count();
      $dsewa_first = DSewa::where('h_sewa_id',$hsewa->id)->first();
      $barang_detail_first = BarangDetail::where('id',$dsewa_first->barang_detail_id)->first();
      $barang_id = $barang_detail_first->barang_id;
      $barang = Barang::find($barang_id);
      $barang->stok_available = $barang->stok_available+$total_stok;
      $barang->stok_onbook = $barang->stok_onbook-$total_stok;
      $barang->save();
    }
    $hsewa->save();
    // sendNotificationToDevice($message->user_from->nama, $message->value, $message->user_to->firebase_fcm_token, $sendData);
    $this->sendNotificationToDevice('Status Pesanan','Pesanan atas invoice '.$hsewa->kodenota.' anda '.$status_name, $hsewa->user_peminjam->firebase_fcm_token, []);
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getDaftarBarangList(Request $request){
    $input = $request->all();
    $data = Barang::with(['user','barang_fotos','barang_harga_sewas','barang_harga_sewa_utama','barang_foto_utama','barang_sub_kategori.barang_kategori'])
    ->where('users_id', '=', Auth::id())
    ->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getReportTransaksiListFromFilter(Request $request){
    $input = $request->all();
    $filter = json_decode($request->input('filter'),true);
    $request->merge($filter);
    $hsewa = HSewa::with(['d_sewa_utama.barang_detail.barang.barang_foto_utama'])
    ->has('d_sewa_utama.barang_detail.barang.barang_foto_utama')
    ->where('users_penyewa_id',Auth::id());
    if ($request->input('tipe')=='selesai'){
      $hsewa = $hsewa->whereNotNull('tgl_diterima_penyewa_kembali');
    } else {
      $hsewa = $hsewa->whereNull('tgl_diterima_penyewa_kembali');
    }
    $hsewa = $hsewa->orderBy('created_at','desc');
    $hsewa = $hsewa->get();
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  // public function getReportBarangDisewa(Request $request){
  //   $input = $request->all();
  //   $data = BarangDetail::with(['barang'])
  //   ->whereHas('barang', function($query){
  //     $query->where('users_id', Auth::id());
  //   })
  //   ->where('status_barang', '3')
  //   ->get();
  //   return response()->json($this->setSuccessResponse($data,$input));
  // }

}
