<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Posting;
use App\Models\Komentar;
use App\Models\User;
use App\Models\FollowList;
use App\Models\Rating;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PostController extends Controller
{
    public function tooglefollow(Request $request)
    {
        $follow = FollowList::where([
            ['users_id1','=',$request->input('users_id')],
            ['users_id','=',$request->user()->id]
        ])->get();
        if ($follow->isEmpty())
        {
            $addFollow = new FollowList();
            $addFollow->users_id = $request->user()->id;
            $addFollow->users_id1 = $request->input('users_id');
            $addFollow->save();
        }
        else {
            FollowList::destroy($follow->toArray());
        }
        return $this->usersWithFollow($request);
    }
    public function addStars(Request $request)
    {

    }
    public function usersWithFollow(Request $request)
    {
        $users_notfollow = User::select('users.*',DB::raw('0 as followed'))
        ->whereNotExists(function ($query) {
            $query->select(DB::raw('1'))
            ->from('follow_list')
            ->whereRaw('follow_list.users_id1 = users.id');
        })
        ->where('users.id','<>',$request->user()->id)
        ->get();
        $users_follow = User::select('users.*',DB::raw('1 as followed'))
        ->whereExists(function ($query) {
            $query->select(DB::raw('1'))
            ->from('follow_list')
            ->whereRaw('follow_list.users_id1 = users.id');
        })
        ->where('users.id','<>',$request->user()->id)
        ->get();
        // if ($users_follow->isEmpty()) return response()->json(['data' => $users_notfollow], 200, [], JSON_NUMERIC_CHECK);
        // if ($users_notfollow->isEmpty()) return response()->json(['data' => $users_follow], 200, [], JSON_NUMERIC_CHECK);
        $users = $users_follow->merge($users_notfollow);
        return response()->json(['data' => $users], 200, [], JSON_NUMERIC_CHECK);
    }
	public function userProfile(Request $request)
	{
		$profile = User::with(['postings','following_lists','follower_lists'])->find($request->user()->id);
		$profile->follower_count = $profile->follower_lists->count();
		$profile->following_count = $profile->following_lists->count();
		$profile->posting_count = $profile->postings->count();
        return response()->json(['data' => $profile], 200, [], JSON_NUMERIC_CHECK);
	}
    public function addPosting(Request $request)
    {
        $date = Carbon::now();
        $post = new Posting();
        $post->caption = $request->input('caption');
		$post->timestamp = $date->toDateTimeString();
		$post->lat = $request->input('lat');
		$post->lng = $request->input('lng');
		$post->photourl = $request->input('photourl');
		$post->location = $request->input('location');
		$post->camera_maker = $request->input('camera_maker');
		$post->camera_model = $request->input('camera_model');
		$post->fstop = $request->input('fstop');
		$post->exposure_time = $request->input('exposure_time');
		$post->isospeed = $request->input('isospeed');
        $post->user_id = $request->user()->id;
        $post->save();
        return $this->posting($request);
    }
    public function updateUserProfile(Request $request)
    {
        $user = User::find($request->user()->id);
        $user->name = $request->input('name');
        $user->gender =  $request->input('gender');
        $user->biodata =  $request->input('biodata');
        $user->alamat =  $request->input('alamat');
        $user->save();
        return $this->userProfile($request);
    }
    public function posting(Request $request)
    {
        $posts = Posting::with('user', 'komentars.user', 'ratings')->orderBy('timestamp', 'desc')->get(); //select * from posting, user where posting.user_id = user.id
        // return $posts[0]->ratings->sum('bintang');
        foreach ($posts as $key => $post) {
            // $user_rating = $post->ratings->where('users_id',$request->user()->id)->first();
            // $user_rating = (is_null($user_rating) ? '0' : $user_rating['bintang']);
            // $posts[$key]->current_user_rating = $user_rating;
            $posts[$key]->current_user_rating = $this->getUserRating($post,$request);
        }
        return response()->json(['data' => $posts], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getUserRating($post, Request $request)
    {
        $user_rating = $post->ratings->where('users_id',$request->user()->id)->first();
        $user_rating = (is_null($user_rating) ? '0' : $user_rating['bintang']);
        return $user_rating;
    }
    public function postingId(Request $request)
    {
        //$posts = Posting::with('user','komentars.user','ratings')->whereId($request->input('posting_id'))->get();
        $post = Posting::with('user', 'komentars.user', 'ratings')->find($request->input('posting_id'));
        $post->current_user_rating = $this->getUserRating($post,$request);
        return response()->json(['data' => $post], 200, [], JSON_NUMERIC_CHECK);
    }
    public function setPostingRating(Request $request)
    {
        $rating = Rating::where([
            ['posting_id','=',$request->input('posting_id')],
            ['users_id','=',$request->user()->id]
        ]);

        if ($rating->get()->isEmpty())
        {
            $addRating = new Rating();
            $addRating->posting_id = $request->input('posting_id');
            $addRating->users_id = $request->user()->id;
            $addRating->bintang = $request->input('stars');
            $addRating->save();
        }
        else {
            $rating->update(['bintang' => $request->input('stars')]);
        }
        return $this->postingId($request);
    }

    public function addComment(Request $request)
    {
        $komentar = new Komentar();
        $komentar->posting_id = $request->input('posting_id');
        $komentar->isi_komentar = $request->input('comment');
        $komentar->users_id = $request->user()->id;
        $komentar->save();
        return $this->postingId($request);
        // $posts = Posting::with('user','komentars.user','ratings')->find($request->input('posting_id'));
        // return response()->json(['data' => $posts], 200, [], JSON_NUMERIC_CHECK);
    }

    public function indextest(Request $request)
    {
        $posts = User::get();
        return $posts;
    }
    public function indextestauth(Request $request)
    {
        //update insert delete
        return $request->user();
    }
}
