<?php
namespace App\Http\Controllers\Api\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Midtrans;

class ApiTransaction extends Controller
{
  public function postRent(Request $request) {
    $transaction_details = [
        'order_id' => time(),
        'gross_amount' => 10000
    ];

    $customer_details = [
        'first_name' => 'User',
        'email' => 'user@gmail.com',
        'phone' => '08238493894'
    ];

    $custom_expiry = [
        'start_time' => date("Y-m-d H:i:s O", time()),
        'unit' => 'day',
        'duration' => 2
    ];

    $item_details = [
        'id' => 'PROD-1',
        'quantity' => 1,
        'name' => 'Product-1',
        'price' => 10000
    ];

    // Send this options if you use 3Ds in credit card request
    $credit_card_option = [
        'secure' => true,
        'channel' => 'migs'
    ];

    $transaction_data = [
        'transaction_details' => $transaction_details,
        'item_details' => $item_details,
        'customer_details' => $customer_details,
        'expiry' => $custom_expiry,
        'credit_card' => $credit_card_option,
    ];

    $token = Midtrans::getSnapToken($transaction_data);
    return $token;
  }

  public function postOpenToko(Request $request)
  {
    $input = $request->all();
    try
    {
      $user = User::findOrFail(Auth::id());
      $user->is_vendor = 1;
      $user->save();
      // $data = $user->makeVisible('api_token');
      $data = $user;
      return response()->json($this->setSuccessResponse($data,$input));
    }
    catch(ModelNotFoundException $e)
    {
      $error = [];
      return response()->json($this->setErrorResponse($error,$input,'502',$e->getMessage()));
    }
  }
}
