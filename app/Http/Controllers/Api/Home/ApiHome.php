<?php
namespace App\Http\Controllers\Api\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\Carousel;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ApiHome extends Controller
{
  public function index(Request $request) {

  }
  public function getData(Request $request) {
    // return $request->all();
    $input = $request->all();
    $barang = Barang::with(['user','barang_harga_sewa_utama','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori','wishlisted'])
    ->where('users_id','<>',$request->input('id'))
    ->where('stok_available','>','0')
    // ->orderBy('created_at','desc')
    ->get();
    $barang = $barang->sortByDesc(function($brg){
      return $brg->total_rating;
    });
    $barang = $barang->values()->all();
    $user = User::find($request->input('id'));
    if ($user){
      $user = $user->append('total_saldo');
    }
    $carousel = Carousel::get();
    $kategori = BarangKategori::get();
    $subkategori = BarangSubKategori::get();
    $data = [
      'user' => $user,
      'barang' => $barang,
      'carousel' => $carousel,
      'kategori' => $kategori,
      'subkategori' => $subkategori,
    ];
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getItemLatest(Request $request) {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori','barang_harga_sewas','barang_harga_sewa_utama'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemList(Request $request) {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori','barang_harga_sewas','barang_harga_sewa_utama'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemDetail(Request $request) {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori','barang_harga_sewas','barang_harga_sewa_utama'])->find($request->input('id'));
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
}
