<?php
namespace App\Http\Controllers\Api\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\HSewa;
use App\Models\DSewa;
use App\Models\ReviewBarang;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\FotoReview;

class ApiMember extends Controller
{
  public function getTransactionDataFromId(Request $request){
    $input = $request->all();
    $hsewa = HSewa::has('h_piutangs')
    ->with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_peminjam_id',Auth::id())
    ->where('status_transaksi','!=','0')
    ->find($request->input('id'));
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getTransactionList(Request $request){
    $input = $request->all();
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_peminjam_id',Auth::id())
    ->where('status_transaksi','!=','0')
    ->orderBy('created_at','desc')
    ->get();
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function postTransactionChange(Request $request){
    $input = $request->all();
    $request->merge($request->input('h_sewa'));
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewas','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs'])
    ->where('users_peminjam_id',Auth::id())
    ->find($request->input('id'));

    $hsewa->status_transaksi = $request->input('status_transaksi');
    $status_name = '';
    if ($hsewa->status_transaksi==5) {
      $status_name = 'telah diterima member';
      $hsewa->tgl_diterima_peminjam = Carbon::now();

    } else if ($hsewa->status_transaksi==6) {
      $status_name = 'telah dikembalikan member';
      $hsewa->tgl_diterima_penyewa_kembali = Carbon::now();
      
    } else if ($hsewa->status_transaksi==7) {
      $status_name = 'sudah selesai';
    }
    $hsewa->save();
    $this->sendNotificationToDevice('Status Pesanan','Pesanan atas invoice '.$hsewa->kodenota.' anda '.$status_name, $hsewa->user_penyewa->firebase_fcm_token, []);
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getReviewBarangList(Request $request){
    $input = $request->all();
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs','review_barang_utama.foto_reviews'])
    ->where('users_peminjam_id',Auth::id())
    ->where('status_transaksi','>=','6')
    ->orderBy('created_at','desc')
    ->get();
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getReviewBarangDataFromId(Request $request){
    $input = $request->all();
    $hsewa = HSewa::with(['user_peminjam','user_penyewa','d_sewa_utama.barang_detail.barang.barang_foto_utama','d_sewas.barang_detail.barang.barang_foto_utama','h_piutang_utama','h_piutangs','pengembalian_deposits','review_barangs.foto_reviews','review_barang_utama.foto_reviews'])
    ->where('users_peminjam_id',Auth::id())
    // ->where('status_transaksi','=','7')
    ->find($request->input('id'));
    
    $data = $hsewa;
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function postReviewBarang(Request $request){
    $input = $request->all();
    $request->merge($request->input('review_barang'));
    Log::info('review : '.json_encode($request->all()));
    $review_barang = new ReviewBarang();
    if ($request->has('id')){
      $review_barang = ReviewBarang::find($request->input('id'));
    }
    DB::transaction(function () use($request, $review_barang) {
      $edit = false;
      if ($request->has('id')){
        $edit = true;
      }
      $hsewa = HSewa::where('users_peminjam_id',Auth::id())
      ->find($request->input('h_sewa_id'));

      $review_barang->rating = $request->input('rating');
      $review_barang->komentar = $request->input('komentar');
      $review_barang->users_id = Auth::id();
      $review_barang->barang_id = $request->input('barang_id');
      $review_barang->h_sewa_id = $hsewa->id;
      $review_barang->save();

      $foto_reviews = [];
      foreach ($request->input('foto_reviews') as $key => $foto) {
        $foto_review = new FotoReview([
          'foto_url' => $foto['foto_url'],
        ]);
        if (array_key_exists('id', $foto)) {
          $foto_review->id = $foto['id'];
        };
        $foto_reviews[] = $foto_review;
      }
      if ($edit){
        $review_barang->foto_reviews()->forceDelete();
      }
      $review_barang->foto_reviews()->saveMany($foto_reviews);
    }, 5);

    return $this->getReviewBarangList($request);

  }
}
