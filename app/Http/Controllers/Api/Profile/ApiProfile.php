<?php
namespace App\Http\Controllers\Api\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UsersBank;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\HPiutang;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\Home\ApiHome;

class ApiProfile extends Controller
{
  public function getInfo(Request $request)
  {
    $input = $request->all();
    try
    {
      $user = User::has('h_sewas_penyewa.d_sewa_utama')
      ->with(['barangs','h_piutangs_complete','h_sewas_penyewa.d_sewa_utama.barang_detail.barang.barang_foto_utama'])
      // .barang_detail.barang.barang_foto_utama
      ->findOrFail(Auth::id());
      // return $user;
      if ($user){
        $user = $user->append('total_saldo');
      }
      $data = $user;
      return response()->json($this->setSuccessResponse($data,$input));
    }
    catch(ModelNotFoundException $e)
    {
      $error = [];
      return response()->json($this->setErrorResponse($error,$input,'502',$e->getMessage()));
    }
  }
  public function postSaldoWithdraw(Request $request){
    $input = $request->all();
    $date_today = Carbon::today();
    $lp_ven = HPiutang::where('tgl','=',$date_today)->count()+1;
    $kp_ven = $this->generateCode('CDP',$date_today,$lp_ven);
    $p_ven = new HPiutang();
    $p_ven->kodenota = $kp_ven;
    $p_ven->tgl = $date_today;
    $p_ven->total_bayar = $request->input('nominal')*-1;
    $p_ven->payment_method = 'cair_dana';
    $p_ven->status_pembayaran = 5;
    $p_ven->users_id = Auth::id();
    $p_ven->h_sewa_id = null;
    $p_ven->save();
    $home = new ApiHome();
    $request->merge(['id'=>Auth::id()]);
    return $home->getData($request);
    // $this->sendMessageFromAdmin('Transaksi atas '.$hsewa->kodenota.' telah selesai dan dana anda telah ditransfer ke rekening anda ('.$hsewa->user_penyewa->bank_utama->nomor.') a.n ('.$hsewa->user_penyewa->bank_utama->nama.') melalui bank '.$hsewa->user_penyewa->bank_utama->bank->nama,'claim',Auth::id(),$hsewa->users_penyewa_id);
    
  }
  public function postVerification(Request $request){
    $input = $request->all();
    $request->merge($request->input('user'));
    Log::info('user : '.json_encode($request->all()));
    // return '';
    $data = User::find(Auth::id());
    $data->nama = $request->input('nama');
    $data->alamat = $request->input('alamat');
    $data->no_telp = $request->input('no_telp');
    $data->foto_ktp_url = $request->input('foto_ktp_url');
    $data->pin_alamat = $request->input('pin_alamat');
    $data->pin_latitude = $request->input('pin_latitude');
    $data->pin_longitude = $request->input('pin_longitude');
    $data->kode_pos = $request->input('kode_pos');
    $data->kecamatan_id = $request->input('kecamatan_id');
    $data->no_ktp = $request->input('no_ktp');
    $data->tgl_lahir = Carbon::parse($request->input('tgl_lahir'));
    $data->verified = Carbon::now();
    if ($request->has('users_bank_utama')) {
      $users_bank = UsersBank::where('users_id',Auth::id())->delete();
      $bank_utama = $request->get('users_bank_utama');
      $users_bank = new UsersBank();
      $users_bank->nomor = $bank_utama['nomor'];
      $users_bank->nama = $bank_utama['nama'];
      $users_bank->bank_id = $bank_utama['bank_id'];
      $users_bank->users_id = Auth::id();
      $users_bank->utama = 1;
      $users_bank->save();
    }
    $data->save();
    return response()->json($this->setSuccessResponse($data,$input));
  }
}
