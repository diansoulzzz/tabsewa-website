<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Validator;
// use Socialite;
use FirebaseServiceProvider;
use Carbon\Carbon;
use Firebase\Auth\Token\Exception\InvalidToken;

class ApiLoginController extends Controller
{
  public function authFirebase(Request $request)
  {
    $fb_auth = [];
    try {
      $idTokenString = $request->input('firebase_token');
      $firebase = app('firebase');
      $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
      $uid = $verifiedIdToken->getClaim('sub');
      $fb_auth = $firebase->getAuth()->getUser($uid);
      // return $fb_auth;
      // return Carbon::createFromTimestamp($fb_auth->tokensValidAfterTime->getTimestamp());
      // return response()->json(['data' => $user,'message'=> $message,'status'=> $status,'access_token'=>$accessToken]);
    } catch (InvalidToken $e) {
      $error = [];
      $input = $request->all();
      return response()->json($this->setErrorResponse($error,$input,'502',$e->getMessage()));
      // return $e->getMessage();
    }

    $user = User::where('firebase_auth_uid', $fb_auth->uid)->first();
    $message = '';
    $status = '';
    if (!$user)
    {
      $user = new User();
      $nama = isset($fb_auth->displayName)?$fb_auth->displayName:'Tak Sewa';
      $user->nama = $nama;
      // $user->email = isset($fb_auth->displayEmail)?$fb_auth->displayEmail:'';
      $user->email = isset($fb_auth->email)?$fb_auth->email:'';
      $user->no_telp = isset($fb_auth->phoneNumber)?$fb_auth->phoneNumber:'';
      $user->foto_url = 'https://ui-avatars.com/api/?name='.str_slug($nama,'+').'&rounded=true&background='.$this->random_color().'&color=fff&size=128&length=2';
      // isset($fb_auth->photoUrl)?$fb_auth->photoUrl:'';
      $user->firebase_auth_token = $idTokenString;
      $user->firebase_auth_uid = $fb_auth->uid;
      $user->firebase_auth_provider_id = isset($fb_auth->providerData[0]->providerId)?$fb_auth->providerData[0]->providerId:'';
      $user->firebase_auth_password_hash = isset($fb_auth->passwordHash)?$fb_auth->passwordHash:'';
      $user->firebase_auth_token_valid_at = isset($fb_auth->tokensValidAfterTime)?Carbon::createFromTimestamp($fb_auth->tokensValidAfterTime->getTimestamp()):'';
      $user->firebase_auth_last_login_at = isset($fb_auth->metadata->lastLoginAt)?Carbon::createFromTimestamp($fb_auth->metadata->lastLoginAt->getTimestamp()):'';
      $user->aktif = 1;
      $user->api_token = str_random(60);
      $user->save();
      $user = User::find($user->id);
    }
    if (!$user->verified)
    {
      $message = 'Need Verified';
      $status = '101';
      $accessToken = 'Bearer '.$user->api_token;
    }
    $data = $user->makeVisible('api_token');
    $input = $request->all();
    return response()->json($this->setSuccessResponse($data,$input,$status,$message));
  }
}
