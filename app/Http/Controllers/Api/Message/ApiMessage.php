<?php
namespace App\Http\Controllers\Api\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\HSewa;
use App\Models\Message;
use Carbon\Carbon;

class ApiMessage extends Controller
{
  public function getMessageRoom(Request $request){
    $input = $request->all();

    $m_to = Message::select(DB::raw('users_id_to as u_to'), DB::raw('MAX(id) as last_id'))->where('users_id_from',Auth::id())->groupBy('u_to');
    $m_fr = Message::select(DB::raw('users_id_from as u_to'), DB::raw('MAX(id) as last_id'))->where('users_id_to',Auth::id())->groupBy('u_to');
    $m_to = $m_to->union($m_fr);

    $m_max = Message::select(DB::raw('m_to.u_to as u_to'), DB::raw('MAX(m_to.last_id) as m_last_id'))
    ->joinSub($m_to, 'm_to', function ($join) {
      $join->on('message.id', '=', 'm_to.last_id');
    })
    ->groupBy('m_to.u_to');
    // return $m_max->get();
    $message = Message::select(DB::raw('m_max.u_to as users_id_to'),'message.id','message.value','message.type','message.created_at','message.updated_at','message.deleted_at',DB::raw('"'.Auth::id().'" as users_id_from'))
    ->joinSub($m_max, 'm_max', function ($join) {
      $join->on('message.id', '=', 'm_max.m_last_id');
    });
    $data = $message->with(['user_from','user_to'])->orderBy('updated_at','desc')->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function get10MessageFromId(Request $request){
    $input = $request->all();
    $data = [];
    $users_id_to = $request->get('users_id_to');
    $id = $request->get('id');
    if ($id != 0){
      $last_message = Message::find($id);
      if ($last_message) {

        $messageList = Message::with(['user_from','user_to'])->where('id','<',$last_message->id)
        ->where(function ($query) use ($users_id_to) {
          $query->where('users_id_from',Auth::id())->where('users_id_to',$users_id_to);
        })
        ->Orwhere(function ($query) use ($users_id_to) {
          $query->where('users_id_to',Auth::id())->where('users_id_from',$users_id_to);
        })
        ->orderBy('id','desc')->get();
        $data = $messageList->take(20);
      }
    } else {
      $messageList = Message::with(['user_from','user_to'])->where(function ($query) use ($users_id_to) {
        $query->where('users_id_from',Auth::id())->where('users_id_to',$users_id_to);
      })
      ->Orwhere(function ($query) use ($users_id_to) {
        $query->where('users_id_to',Auth::id())->where('users_id_from',$users_id_to);
      })
      ->orderBy('id','desc')->get();
      $messageListDated = $messageList->take(20);
      $list = [];
      $last_date_loop = '';
      $isShowDate = 0;
      $messageListDated = $messageListDated->reverse();
      foreach ($messageListDated as $key => $value) {
        if ($value->created_at->toDateString() != $last_date_loop) {
          $isShowDate = 1;
        } else {
          $isShowDate = 0;
        }
        $value->is_show_date = $isShowDate;
        $list[]=$value;
        $last_date_loop = $value->created_at->toDateString();
      }
      $list = collect($list)->reverse()->values()->all();
      $data = $list;
      // return $data;
    }
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function postEntry(Request $request){
    $input = $request->all();
    $request->merge($request->input('message'));
    $message = new Message();
		$message->value = $request->input('value');
		$message->type = $request->input('type');
		$message->is_read = $request->input('is_read');
		$message->uuid = $request->input('uuid');
		$message->users_id_from = Auth::id();
		$message->users_id_to = $request->input('users_id_to');
    $message->save();
    $data = $message;
    $message = Message::with(['user_from','user_to'])->find($message->id);
    if ($message->user_to->firebase_fcm_token!= null){
      $sendData = [
        'intent'=>(string) 'message',
        'value'=> (string) $message->value,
        'type'=> (string) $message->type,
        'is_read'=> (string) $message->is_read,
        'uuid'=> (string) $message->uuid,
        'users_id_from'=> (string) $message->users_id_from,
        'users_id_to'=> (string) $message->users_id_to,
        'users_nama_from'=> (string) $message->user_from->nama,
        'users_foto_url_from'=> (string) $message->user_from->foto_url,
      ];
      $this->sendNotificationToDevice($message->user_from->nama, $message->value, $message->user_to->firebase_fcm_token, $sendData,'OPEN_MESSAGE_ACTIVITY');
    }
    // $data = Message::find($message->id);
    return response()->json($this->setSuccessResponse($data,$input));
  }
}
