<?php
namespace App\Http\Controllers\Api\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\Kotum;
use App\Models\Kecamatan;
use App\Models\Provinsi;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ApiSearch extends Controller
{
  // public static int SORT_BY_RELEVANCE = 0;
  // public static int SORT_BY_REVIEW = 1;
  // public static int SORT_BY_NEW_ITEM = 2;
  // public static int SORT_BY_LOW_PRICE = 3;
  // public static int SORT_BY_HIGH_PRICE = 4;
  public function getBarang(Request $request) {
    $SORT_BY_RELEVANCE = 0;
    $SORT_BY_REVIEW = 1;
    $SORT_BY_NEW_ITEM = 2;
    $SORT_BY_LOW_PRICE = 3;
    $SORT_BY_HIGH_PRICE = 4;

    $input = $request->all();
    $query = json_decode($request->input('query'),true);
    $request->merge($query);
    Log::info('search : '.json_encode($request->all()));
    $queryString = $request->get('query');
    $filter ='';
    if ($request->has('filter'))
    {
      $filter = $request->get('filter');
    }
    // return $filter;
    $subkategori = '%';
    if ($request->has('subkategori'))
    {
      $subkategori = $request->get('subkategori');
    }
    $sort = 0;
    if ($request->has('sort'))
    {
      $sort = $request->get('sort');
    }
    if ($sort==$SORT_BY_NEW_ITEM){ //terbaru
      $order['field'] = 'barang.created_at';
      $order['sc'] = 'desc';
    } else if ($sort==$SORT_BY_HIGH_PRICE){ //termahal
      $order['field'] = 'barang_harga_sewa.harga';
      $order['sc'] = 'desc';
    } else if ($sort==$SORT_BY_LOW_PRICE){ //termurah
      $order['field'] = 'barang_harga_sewa.harga';
      $order['sc'] = 'asc';
    }
    $filter_kategori = [];
    $filter_sub_kategori = [];
    $filter_kota = [];
    if ($request->has('filter.filter_barang_kategori_ids')){
      $filter_kategori = collect($filter['filter_barang_kategori_ids'])->pluck('id');
    }
    if ($request->has('filter.filter_barang_sub_kategori_ids')){
      $filter_sub_kategori = collect($filter['filter_barang_sub_kategori_ids'])->pluck('id');
    }
    if ($request->has('filter.filter_kota_ids')){
      $filter_kota = collect($filter['filter_kota_ids'])->pluck('id');
    }
    $barang = Barang::join('barang_sub_kategori','barang_sub_kategori.id','=','barang.barang_sub_kategori_id')
    ->join('users','users.id','=','barang.users_id')
    ->leftJoin('kecamatan','kecamatan.id','=','users.kecamatan_id')
    ->join('barang_kategori','barang_kategori.id','=','barang_sub_kategori.barang_kategori_id')
    ->join('barang_harga_sewa', function ($join) {
      $join->on('barang_harga_sewa.barang_id','=','barang.id')
     ->where('barang_harga_sewa.lama_hari','=','1');
    })
    ->select('barang.*')
    ->where('barang_sub_kategori.id','like',$subkategori);
    if ($filter_sub_kategori->count()>0){
      $barang->whereIn('barang_sub_kategori.id', $filter_sub_kategori);
    }
    if ($filter_kategori->count()>0){
      $barang->whereIn('barang_kategori.id', $filter_kategori);
    }
    if ($filter_kota->count()>0){
      $barang->whereIn('kecamatan.kota_id', $filter_kota);
    }
    // ->where('barang.users_id','<>',$request->input('id')) //remark users
    $barang->where(function ($query) use ($queryString) {
      $query->where('barang.nama','like','%'.$queryString.'%')
      ->orWhere('barang_sub_kategori.nama','like','%'.$queryString.'%')
      ->orWhere('barang_kategori.nama','like','%'.$queryString.'%');
    });
    if ($filter){
      if ($filter['harga_max']>0){
        $barang = $barang->where('barang_harga_sewa.harga','<=',$filter['harga_max']);
      }
      if ($filter['harga_min']>0){
        $barang = $barang->where('barang_harga_sewa.harga','>=',$filter['harga_min']);
      }
    }
    if (($sort) && ($sort<>$SORT_BY_RELEVANCE) && ($sort<>$SORT_BY_REVIEW)){
      $barang = $barang->orderBy($order['field'], $order['sc']);
    }
    $barang = $barang->with(['user','barang_foto_utama','barang_sub_kategori.barang_kategori','barang_harga_sewas','barang_harga_sewa_utama','barang_review.foto_reviews'])->get();
    $barang_sub_kategori = $barang->pluck('barang_sub_kategori');
    $barang_kategori = $barang_sub_kategori->pluck('barang_kategori');
    $barang_sub_kategori = $barang_sub_kategori->flatten()->unique()->values()->all();
    $barang_kategori = $barang_kategori->flatten()->unique()->values()->all();
    if (($sort) && ($sort==$SORT_BY_REVIEW)){
      $barang = $barang->sortByDesc(function($brg){
        return $brg->total_rating;
      });
      $barang = $barang->values()->all();
    }
    if ($request->has('filter.rating')){
      $min_rating = $filter['rating'];
      if ($min_rating>0){
        $barang = $barang->where('total_rating','>=',$min_rating);
      }
      $barang = $barang->values()->all();
    }
    $barang_kategori = BarangKategori::has('barang_sub_kategoris.barangs')->select('id','nama')->get();
    $barang_sub_kategori = BarangSubKategori::has('barangs')->select('id','nama')->get();
    $kota = Kotum::has('kecamatans.users.barangs')->select('id','nama')->get();
    $data= [
      'barang' => $barang,
      'barang_kategori' => $barang_kategori,
      'barang_sub_kategori' => $barang_sub_kategori,
      'kota' => $kota,
      // 'provinsi' => Provinsi::select('id','nama')->get(),
    ];
    // $data= [
    //   'barang' => $barang,
    //   'barang_kategori' => $barang_kategori->flatten()->unique()->values()->all(),
    //   'barang_sub_kategori' => $barang_sub_kategori->flatten()->unique()->values()->all(),
    // ];
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getVendor(Request $request){
    $input = $request->all();
    $query = json_decode($request->input('query'),true);
    $request->merge($query);
    $queryString = $request->get('query');
    $filter ='';
    if ($request->has('filter'))
    {
      $filter = $request->get('filter');
    }
    $vendor = User::select(['id','nama','foto_url','created_at'])->where('is_vendor',1)
    ->where(function ($query) use ($queryString) {
      $query->where('nama','like','%'.$queryString.'%');
    })
    ->get();
    $data= [
      'vendor' => $vendor,
    ];
    return response()->json($this->setSuccessResponse($data,$input));
  }


  // public function getSearch(Request $request)
  // {
  //   $sort = 0;
  //   if ($request->has('sort'))
  //   {
  //     $sort = $request->get('sort');
  //   }
  //   if ($sort==0){ //terbaru
  //     $order['field'] = 'barang.created_at';
  //     $order['sc'] = 'desc';
  //   } else if ($sort==1){ //termahal
  //     $order['field'] = 'barang_harga_sewa.harga';
  //     $order['sc'] = 'desc';
  //   } else if ($sort==2){ //termurah
  //     $order['field'] = 'barang_harga_sewa.harga';
  //     $order['sc'] = 'asc';
  //   }
  //   $subkategori = '%';
  //   if ($request->has('subkategori'))
  //   {
  //     $subkategori = $request->get('subkategori');
  //   }
  //   $search = $request->get('query');
  //   // $barang = Barang::with(['barang_sub_kategori.barang_kategori','barang_foto_utama','user'])
  //   // ->orWhere('nama','like','%'.$search.'%')
  //   // ->orWhereHas('barang_sub_kategori', function ($query) use ($subkategori, &$search) {
  //   //   $query->where('id','like',$subkategori);
  //   //   $query->orWhere('nama','like','%'.$search.'%');
  //   // })->toSql();
  //   // ->get();
  //
  //   // $barang = Barang::get();
  //   // return $barang;
  //
  //   $barang = Barang::join('barang_sub_kategori','barang_sub_kategori.id','=','barang.barang_sub_kategori_id')
  //   // ->join('barang_harga_sewa','barang_harga_sewa.barang_id','=','barang.id')
  //   ->join('barang_harga_sewa', function ($join) {
  //     $join->on('barang_harga_sewa.barang_id','=','barang.id')
  //    ->where('barang_harga_sewa.lama_hari','=','1');
  //   })
  //   ->select('barang.*')
  //   ->where('barang_sub_kategori.id','like',$subkategori)
  //   ->where(function ($query) use ($search) {
  //     $query->where('barang.nama','like','%'.$search.'%')
  //     ->orWhere('barang_sub_kategori.nama','like','%'.$search.'%');
  //   })
  //   ->orderBy($order['field'], $order['sc']);
  //
  //   $barang = $barang->with(['user','barang_foto_utama','barang_sub_kategori.barang_kategori','barang_harga_sewas','barang_harga_sewa_utama'])->get();
  //   $data= [
  //     'barang' => $barang,
  //   ];
  //   return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);
  //   // return $barang->get();
  //   // return $barang->toSql();
  // }
}
