<?php
namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;

class ApiBarangKategori extends Controller
{
  public function index(Request $request)
  {

  }
  public function getFromId(Request $request)
  {
    $input = $request->all();
    $data = BarangKategori::with('barang_sub_kategoris.barang_kategori')->first();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getList(Request $request)
  {
    $input = $request->all();
    $data = BarangKategori::with('barang_sub_kategoris.barang_kategori')->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
}
