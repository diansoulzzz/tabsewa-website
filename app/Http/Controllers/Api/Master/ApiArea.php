<?php
namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Kecamatan;
use App\Models\Kotum;
use App\Models\Provinsi;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApiArea extends Controller
{
  public function index(Request $request) {

  }
  public function getKecamatanList(Request $request){
    $input = $request->all();
    $data = Kecamatan::with(['kotum.provinsi'])->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getKotaList(Request $request){
    $input = $request->all();
    $data = Kotum::with(['provinsi'])->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getProvinsiList(Request $request){
    $input = $request->all();
    $data = Provinsi::get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
}
