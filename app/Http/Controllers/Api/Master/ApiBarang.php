<?php
namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangDetail;
use App\Models\BarangFoto;
use App\Models\BarangHargaSewa;
use App\Models\BarangKategori;
use App\Models\LastSeenBarang;
use App\Models\Wishlist;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApiBarang extends Controller
{
  public function index(Request $request) {

  }
  public function getWishlistList(Request $request){
    $input = $request->all();
    $data = Barang::with(['user','barang_fotos','barang_harga_sewas','barang_harga_sewa_utama','barang_foto_utama','barang_sub_kategori.barang_kategori'])
    ->whereHas('wishlists', function ($query) {
      $query->where('users_id', '=', Auth::id());
    })
    ->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function postToogleWishlistFromId(Request $request){
    // $input = $request->all();
    $data = Wishlist::where('barang_id',$request->input('id'))->where('users_id',Auth::id())->first();
    if ($data){
      $data->delete();
    } else {
      $data = new Wishlist();
      $data->barang_id = $request->input('id');
      $data->users_id = Auth::id();
      $data->created_at = Carbon::now();
      $data->save();
    }
    return $this->getWishlistFromId($request);
    // $data = Wishlist::where('barang_id',$request->input('id'))->where('users_id',Auth::id())->first();
    // return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getWishlistFromId(Request $request) {
    $input = $request->all();
    $data = Wishlist::where('barang_id',$request->input('id'))->where('users_id',Auth::id())->first();
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function getLastSeenBarangList(Request $request){
    $input = $request->all();
    $data = Barang::with(['user','barang_fotos','barang_harga_sewas','barang_harga_sewa_utama','barang_foto_utama','barang_sub_kategori.barang_kategori'])
    ->whereHas('last_seen_barangs', function ($query) {
      $query->where('users_id', '=', Auth::id());
    })
    ->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }

  public function postToogleLastSeenBarangFromId(Request $request){
    $data = LastSeenBarang::where('barang_id',$request->input('id'))->where('users_id',Auth::id())->first();
    if ($data){
      $data->created_at = Carbon::now();
      $data->save();
    } else {
      $data = new LastSeenBarang();
      $data->barang_id = $request->input('id');
      $data->users_id = Auth::id();
      $data->created_at = Carbon::now();
      $data->save();
    }
    return $this->getLastSeenBarangFromId($request);
  }
  public function getLastSeenBarangFromId(Request $request) {
    $input = $request->all();
    $data = LastSeenBarang::where('barang_id',$request->input('id'))->where('users_id',Auth::id())->first();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getFromId(Request $request) {
    $input = $request->all();
    $data = Barang::with(['user','barang_fotos','barang_harga_sewas','barang_harga_sewa_utama','barang_foto_utama','barang_sub_kategori.barang_kategori','wishlisted','barang_review.user','barang_review.foto_reviews'])->find($request->input('id'));
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function getList(Request $request) {
    $input = $request->all();
    $data = Barang::with('barang_sub_kategoris')->get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function postEntry(Request $request){
    // DB::transaction(function () use($request) {
    $input = $request->all();
    $barang = new Barang();
    $barang->nama = $request->input('nama');
    $barang->deposit_min = $request->input('deposit_min');
    $barang->deskripsi = $request->input('deskripsi');
    $barang->stok = $request->input('stok');
    $barang->barang_sub_kategori_id = $request->input('barang_sub_kategori_id');
    $barang->users_id = Auth::id();
    $barang->stok_available = $barang->stok;
    $barang->stok_onbook = 0;
    $barang->save();
    $barang_fotos = [];
    foreach ($request->input('barang_fotos') as $key => $foto) {
      $barang_fotos[] = new BarangFoto([
        'foto_url' => $foto['foto_url'],
        'utama' => $foto['utama'],
      ]);
    }
    $barang->barang_fotos()->saveMany($barang_fotos);
    $barang_harga_sewas = [];
    foreach ($request->input('barang_harga_sewas') as $key => $harga_sewa) {
      $barang_harga_sewas[] = new BarangHargaSewa([
        'harga' => $harga_sewa['harga'],
        'lama_hari' => $harga_sewa['lama_hari'],
      ]);
    }
    $barang->barang_harga_sewas()->saveMany($barang_harga_sewas);
    $barang_details = [];
    for ($i=0; $i < $request->input('stok'); $i++) {
      $barang_details[] = new BarangDetail([
        'barang_id' => $barang->id,
        'status_barang' => 1,
      ]);
    }
    $barang->barang_details()->saveMany($barang_details);
    $data = $barang;
    return response()->json($this->setSuccessResponse($data,$input));
    // }, 5);
    // $input = $request->all();
    // return response()->json($this->setErrorResponse($error,$input,'502',$e->getMessage()));
    // return Barang::first();
  }
}
