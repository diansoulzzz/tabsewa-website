<?php
namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangFoto;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use App\Models\Bank;
use App\Models\UsersBank;


class ApiMaster extends Controller
{
  public function getBankList(Request $request){
    $input = $request->all();
    $data = Bank::get();
    return response()->json($this->setSuccessResponse($data,$input));
  }
  public function postData(Request $request)
  {
    // $messages = [
    //     'photo_image.required' => 'Foto wajib diisi',
    //     'photo_image.image' => 'File harus image',
    //     'photo_image.dimensions' => 'Panjang dan Tinggi Foto minimal 80 pixel',
    // ];
    // $validator = Validator::make($request->all(), [
    //     'photo_image' => 'required|image|dimensions:min_width=80,min_height=80',
    // ],$messages);
    // if ($validator->fails()) {
    //     return redirect()->back()->withErrorPhoto($validator->errors())->withInput();
    // }
    // if ($request->hasFile('photo_image'))
    // {
    //   $did = Crypt::decryptString($request->input('eid'));
    //   if ($request->file('photo_image')->isValid())
    //   {
    //       $photo_image = $request->file('photo_image');
    //       $users = User::find($did);
    //       Storage::delete($users->photo_url);
    //       $users->photo_url = Storage::disk('public')->put('member/avatars/'.$users->id, $photo_image);
    //       $users->save();
    //       return back()->with('success_photo', 'Foto berasil diubah');
    //   }
    // }
    $barang = new Barang();
    $barang->nama=$request->input('nama');
    $barang->barang_sub_kategori_id=$request->input('barang_sub_kategori_id');
    $barang->users_id=$request->input('users_id');
    $barang->stok=$request->input('stok');
    $barang->deposit_min=$request->input('deposit_min');
    $barang->save();
    foreach ($request->input('foto') as $key => $foto) {
      $barangfoto = new BarangFoto();
      $barangfoto->foto_url = $foto->foto_url;
      $barangfoto->utama = $foto->utama;
      $barangfoto->barang_id = $foto->barang_id;
      $barangfoto->save();
    }
  }
  public function getData(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    $promo = Barang::get();
    $kategori = BarangKategori::get();
    $subkategori = BarangSubKategori::get();
    $data=[
      'barang' => $barang,
      'promo' => $promo,
      'kategori' => $kategori,
      'subkategori' => $subkategori,
    ];
    return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemLatest(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return $this->setSuccessResponse($barang,$request->all());
  }
  public function getItemList(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemDetail(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->find($request->input('id'));
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
}
