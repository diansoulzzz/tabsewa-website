<?php
namespace App\Http\Controllers\Midtrans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangDetail;
use App\Models\BarangFoto;
use App\Models\BarangHargaSewa;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use App\Models\HSewa;
use App\Models\DSewa;
use App\Models\HPiutang;
use Illuminate\Support\Facades\Log;
use Midtrans;
use Veritrans;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MidtransController extends Controller
{
  // public function cancelOrder(Request $request) {
  //   $orderId = $transaction_details->order_id;
  //   HSewa::where('midtrans_order_id',$orderId)->update(['status_transaksi'=>'2']); //2 = CANCEL
  // }
  public function postCharge(Request $request) {
    Log::info('postCharge : '.json_encode($request->all()));
    $token = Midtrans::getSnapToken($request->all());
    // $transaction_object = (object) $request->all();
    // return response()->json($transaction_object);
    //
    DB::transaction(function () use($request) {
      $transaction_object = (object) $request->all();
      $custom1 = (object) json_decode($transaction_object->custom_field1 ,true);
      $custom2 = (object) json_decode($transaction_object->custom_field2 ,true);
      $transaction_details = (object) $transaction_object->transaction_details;
      $tgl_input_peminjaman = Carbon::createFromTimestampMs($custom1->tgl_input_peminjaman)->toDateTimeString();
      $tgl_input_pengembalian = Carbon::createFromTimestampMs($custom1->tgl_input_pengembalian)->toDateTimeString();
      $date_today = Carbon::today();
      $barang = Barang::find($custom2->barang_id);

      $last_invoice = HSewa::where('tgl','=',$date_today)->count()+1;
      $kode_invoice = $this->generateCode('INV',$date_today,$last_invoice);
      $hsewa = new HSewa();
    	$hsewa->kodenota = $kode_invoice;
    	$hsewa->tgl = $date_today;
    	$hsewa->deposit = $custom1->total_deposit;
      $hsewa->biaya_ongkir = $custom1->total_ongkir;
    	$hsewa->grandtotal = $transaction_details->gross_amount;
    	$hsewa->sisabayar = $transaction_details->gross_amount;
    	$hsewa->tgl_input_peminjaman = $tgl_input_peminjaman;
    	$hsewa->tgl_input_pengembalian = $tgl_input_pengembalian;
    	// $hsewa->tgl_konfirmasi_penyewa = Carbon::createFromTimestampMs($transaction_details->tgl_input_peminjaman)->toDateTimeString();
    	// $hsewa->tgl_dikirim_penyewa = Carbon::createFromTimestampMs($transaction_details->tgl_input_peminjaman)->toDateTimeString();
    	// $hsewa->tgl_diterima_peminjam = '';
    	// $hsewa->tgl_diterima_penyewa_kembali = '';
      // $hsewa->keterangan = $transaction_data->custom_field2;
      $hsewa->users_peminjam_id = $transaction_object->user_id;
    	$hsewa->users_penyewa_id = $barang->users_id;
    	$hsewa->status_transaksi = 0;
      $hsewa->midtrans_order_id = $transaction_details->order_id;
      $hsewa->save();

      $barang_details = BarangDetail::with(['barang'])->where('barang_id',$custom2->barang_id)->where('status_barang',1)->get()->take($custom1->total_jml_item);
      $dsewas=[];
      foreach ($barang_details as $key => $barang_detail) {
        $dsewas[] = new DSewa([
          'barang_detail_id' => $barang_detail->id,
          'tgl_input_sewa_awal' => $tgl_input_peminjaman,
          'tgl_input_sewa_akhir' => $tgl_input_pengembalian,
          'lama_sewa' => $custom1->total_lama_sewa,
          'harga_sewa' => $custom2->harga_sewa,
          'subtotal' => $custom2->subtotal_sewa,
        ]);
        BarangDetail::where('id',$barang_detail->id)->update(['status_barang'=>2]);
      }
      $hsewa->d_sewas()->saveMany($dsewas);
      /*
      $barang->stok_available = $barang->stok_available-$custom1->total_jml_item;
      $barang->stok_onbook = $barang->stok_onbook+$custom1->total_jml_item;
      $barang->save();
      */
      // $this->bookingHandle($request);
    }, 5);
    return response()->json($this->getRedirectWithToken($token));
  }

  public function handlingPayment(Request $request){
    DB::transaction(function () use ($request) {
      $transaction_object = (object) $request->all();
      $custom1 = (object) json_decode($transaction_object->custom_field1 ,true);
      $custom2 = (object) json_decode($transaction_object->custom_field2 ,true);
      $tgl_input_peminjaman = Carbon::createFromTimestampMs($custom1->tgl_input_peminjaman)->toDateTimeString();
      $tgl_input_pengembalian = Carbon::createFromTimestampMs($custom1->tgl_input_pengembalian)->toDateTimeString();
      $date_today = Carbon::today();
      $last_piutang = HPiutang::where('tgl','=',$date_today)->count()+1;
      $kode_piutang = $this->generateCode('PIU',$date_today,$last_piutang);
      $hsewa = HSewa::with(['user_peminjam','user_penyewa'])->where('midtrans_order_id',$transaction_object->order_id)->first();
      $hpiutang = HPiutang::where('midtrans_transaction_id',$transaction_object->transaction_id)->first();
      if (!$hpiutang){
        $hpiutang = new HPiutang();
        $hpiutang->kodenota = $kode_piutang;
        $hpiutang->tgl = $date_today;
        $hpiutang->total_bayar = $transaction_object->gross_amount;
        $hpiutang->payment_method = $transaction_object->payment_type;
        $hpiutang->status_pembayaran = 1;
        $hpiutang->users_id = $hsewa->users_penyewa_id;
        $hpiutang->h_sewa_id = $hsewa->id;
        $hpiutang->midtrans_signature_key = $transaction_object->signature_key;
        $hpiutang->midtrans_transaction_id = $transaction_object->transaction_id;
        $hpiutang->midtrans_approval_code = isset($transaction_object->approval_code)?$transaction_object->approval_code:'';
        $hpiutang->midtrans_transaction_status = $transaction_object->transaction_status;
    		$hpiutang->midtrans_payment_code = isset($transaction_object->payment_code) ? $transaction_object->payment_code:'';
    		$hpiutang->midtrans_store = isset($transaction_object->store)?$transaction_object->store:'';
    		$hpiutang->midtrans_status_message = $transaction_object->status_message;
    		$hpiutang->midtrans_status_code = $transaction_object->status_code;
        $hpiutang->save();

        $hsewa = HSewa::find($hsewa->id);
        $hsewa->status_transaksi = 1;
        $hsewa->save();

        $this->sendNotificationToDevice('Menunggu pembayaran',$hsewa->user_peminjam->nama.', Mohon lakukan pembayaran atas invoice '.$hsewa->kodenota, $hsewa->user_peminjam->firebase_fcm_token, []);
      } else {
        $hpiutang = HPiutang::find($hpiutang->id);
        $hpiutang->users_id = $hsewa->users_penyewa_id;
        $hpiutang->midtrans_transaction_id = $transaction_object->transaction_id;
        $hpiutang->midtrans_approval_code = isset($transaction_object->approval_code)?$transaction_object->approval_code:'';
        $hpiutang->midtrans_transaction_status = $transaction_object->transaction_status;
    		$hpiutang->midtrans_status_code = $transaction_object->status_code;
        $hpiutang->save();

        if ($hpiutang->midtrans_transaction_status == 'settlement'){
          // update stok d sini
          /*
          $barang_details = BarangDetail::with(['barang'])->where('barang_id',$custom2->barang_id)->where('status_barang',1)->get()->take($custom1->total_jml_item);
          $dsewas=[];
          foreach ($barang_details as $key => $barang_detail) {
            $dsewas[] = new DSewa([
              'barang_detail_id' => $barang_detail->id,
              'tgl_input_sewa_awal' => $tgl_input_peminjaman,
              'tgl_input_sewa_akhir' => $tgl_input_pengembalian,
              'lama_sewa' => $custom1->total_lama_sewa,
              'harga_sewa' => $custom2->harga_sewa,
              'subtotal' => $custom2->subtotal_sewa,
              // 'subtotal' => ($custom1->total_lama_sewa*$custom2->harga_sewa),
            ]);
            BarangDetail::where('id',$barang_detail->id)->update(['status_barang'=>2]);
          }
          $hsewa->d_sewas()->saveMany($dsewas);
          */
          $dsewas = DSewa::where('h_sewa_id',$hsewa->id)->get();
          foreach ($dsewas as $key => $dsewa) {
            BarangDetail::where('id',$dsewa->barang_detail_id)->update(['status_barang'=>3]);
          }
          $barang = Barang::find($custom2->barang_id);
          $barang->stok_available = $barang->stok_available-$custom1->total_jml_item;
          $barang->stok_onbook = $barang->stok_onbook+$custom1->total_jml_item;
          $barang->save();

          $hsewa = HSewa::find($hsewa->id);
          $hsewa->status_transaksi = 2;
          $hsewa->terbayar = $hsewa->terbayar+$transaction_object->gross_amount;
          $hsewa->sisabayar = $hsewa->sisabayar-$transaction_object->gross_amount;
          $hsewa->save();
          // $hsewa->update(['status_transaksi'=>'1']);
          // kirim notifikasi
          if ($this->sendNotificationToDevice('Invoice telah dibayar','Pembayaran telah dilakukan, order akan diteruskan ke penyewa barang', $hsewa->user_peminjam->firebase_fcm_token, [])) {
              $this->sendNotificationToDevice('Order Baru', $hsewa->user_penyewa->nama.', Ada order baru atas invoice '.$hsewa->kodenota, $hsewa->user_penyewa->firebase_fcm_token, []);
          }
        }
        else if ($hpiutang->midtrans_transaction_status == 'expire') {
          $hsewa = HSewa::find($hsewa->id);
          $hsewa->status_transaksi = -1; //BATAL
          $hsewa->save();
          $this->sendNotificationToDevice('Pembayaran telah expire','Invoice '.$hsewa->kodenota.' telah dibatalkan karena sudah lewat batas pembayaran', $hsewa->user_peminjam->firebase_fcm_token, []);
        }
      }
    }, 5);
  }

  public function getRedirectWithToken($token){
    if (config('midtrans.is_production')){
      return [
          "redirect_url" => "https://app.midtrans.com/snap/v1/transactions/".$token,
          "token" => $token,
      ];
    };
    return [
        "redirect_url" => "https://app.sandbox.midtrans.com/snap/v1/transactions/".$token,
        "token" => $token,
    ];
  }
  public function postNotificationHandling(Request $request) {
    Log::info('postNotificationHandling : '.json_encode($request->all()));
    $this->handlingPayment($request);
  }
  public function postPaymentFinish(Request $request) {
    Log::info('postPaymentFinish : '.json_encode($request->all()));
  }
  public function postPaymentUnfinish(Request $request) {
    Log::info('postPaymentUnfinish : '.json_encode($request->all()));
  }
  public function postPaymentError(Request $request) {
    Log::info('postPaymentError : '.json_encode($request->all()));
  }

  public function postChargeNew(Request $request) {
    $transaction_details = [
        'order_id' => time(),
        'gross_amount' => 10000
    ];
    $customer_details = [
        'first_name' => 'User',
        'email' => 'user@gmail.com',
        'phone' => '08238493894'
    ];
    $custom_expiry = [
        'start_time' => date("Y-m-d H:i:s O", time()),
        'unit' => 'day',
        'duration' => 2
    ];
    $item_details = [
        'id' => 'PROD-1',
        'quantity' => 1,
        'name' => 'Product-1',
        'price' => 10000
    ];
    $credit_card_option = [
        'secure' => true,
        'channel' => 'migs'
    ];
    $transaction_data = [
        'transaction_details' => $transaction_details,
        'item_details' => $item_details,
        'customer_details' => $customer_details,
        'expiry' => $custom_expiry,
        'credit_card' => $credit_card_option,
    ];
    $token = Midtrans::getSnapToken($transaction_data);
    return $token;
  }
}
