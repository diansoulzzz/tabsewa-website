<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;
use FirebaseServiceProvider;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Illuminate\Support\Facades\Log;
use App\Models\Message;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function sendNotificationToDevice($title, $body, $deviceToken, $data=[],$click_action=''){
      Log::info('sendNotificationToDevice : Title = '.$title.", Body = ".$body.", Device = ".$deviceToken." Data = ".json_encode($data)." Action = ".$click_action);
      $isSuccess = false;
      try {
        $firebase = app('firebase');
        $messaging = $firebase->getMessaging();
        $notification = Notification::create()->withTitle($title)->withBody($body);
        $message = CloudMessage::withTarget('token', $deviceToken)->withNotification($notification);
        // $firebase->getMessaging()->validate($message);
        
        if ($click_action<>''){
          $data['click_action'] = (string) $click_action;
        }
        if($data != []) {
          $message = $message->withData($data);
        }
        $messaging->send($message);
        $isSuccess = true;
      } catch (InvalidMessage $e) {
        $isSuccess = false;
      } catch (NotFound $e) {
        $isSuccess = false;
      }
      return $isSuccess;
    }

    public function setSuccessResponse($data=[], $input=[], $code=200, $message='OK'){
      return [
        'data' => $data,
        'input' => $input,
        'status'=> [
          'code' => $code,
          'message' => $message,
        ],
      ];
    }
    public function setErrorResponse($error=[], $input=[], $code=502, $message='Oops theres and error'){
      return [
        'error' => $error,
        'input' => $input,
        'status'=> [
          'code' => $code,
          'message' => $message,
        ],
      ];
    }
    public function generateCode($code, Carbon $date, $counter){
      $morph_counter = sprintf('%05d', $counter);
      return $code.'/'.$date->format('Y/md').'/'.$morph_counter;
    }
    public function random_color_part() {
      return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    public function random_color() {
      return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function sendMessageFromAdmin($value,$type,$u_from,$u_to){
      $message = new Message();
      $message->value = $value;
      $message->type = $type;
      $message->is_read = 1;
      $message->uuid = Str::random(255);
      $message->users_id_from = $u_from;
      $message->users_id_to = $u_to;
      $message->save();
      $data = $message;
      $message = Message::with(['user_from','user_to'])->find($message->id);
      if ($message->user_to->firebase_fcm_token!= null){
        $sendData = [
          'intent'=>(string) 'message',
          'value'=> (string) $message->value,
          'type'=> (string) $message->type,
          'is_read'=> (string) $message->is_read,
          'uuid'=> (string) $message->uuid,
          'users_id_from'=> (string) $message->users_id_from,
          'users_id_to'=> (string) $message->users_id_to,
          'users_nama_from'=> (string) $message->user_from->nama,
          'users_foto_url_from'=> (string) $message->user_from->foto_url,
        ];
        $this->sendNotificationToDevice($message->user_from->nama, $message->value, $message->user_to->firebase_fcm_token, $sendData,'OPEN_MESSAGE_ACTIVITY');
      }
    }
}
