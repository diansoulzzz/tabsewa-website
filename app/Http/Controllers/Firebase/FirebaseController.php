<?php
namespace App\Http\Controllers\Firebase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangDetail;
use App\Models\BarangFoto;
use App\Models\BarangHargaSewa;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use App\Models\HSewa;
use App\Models\DSewa;
use App\Models\HPiutang;
use Illuminate\Support\Facades\Log;
use Midtrans;
use Veritrans;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use FirebaseServiceProvider;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class FirebaseController extends Controller
{
  public function test(Request $request) {
    // ($title, $body, $deviceToken, $data=[]){
    $this->sendNotificationToDevice('tes','tes','fc7HnlOZ1Gk:APA91bHVIKPtbugMd7Pd5F-vv1LDy_1fcWYIFLNXlpfQA73JWRAYi311IhAzNeuJgLmBknHyvFtYxrFvKclt7Cl_Bl_UkZG0Zh48H-1qxn7Wopp3oJK2U_xZ2XtdydTJ0no5Bcf3VdG2',[]);
  }
  public function postFcmToken(Request $request){
    $user = User::find(Auth::id());
    $user->firebase_fcm_token = $request->input('firebase_fcm_token');
    $user->save();
    $data = $user;
    return response()->json($this->setSuccessResponse($data,$request->all()));
  }
  public function index(Request $request){
    $firebase = app('firebase');
    $messaging = $firebase->getMessaging();
    $title = 'Temmy';
    $body = 'Ngarap tem..';
    // $notification = Notification::fromArray([
    //     'title' => $title,
    //     'body' => $body
    // ]);
    // $notification = Notification::create($title, $body);
    $notification = Notification::create()
        ->withTitle($title)
        ->withBody($body);
    // $topic = 'a-topic';
    $data = [
        'first_key' => 'First Value',
        'second_key' => 'Second Value',
    ];
    $deviceToken = 'fYo3KylmGJs:APA91bFGCYRAwcsHc8U1FsIjOafUx5rzbU03l06P9b_9rfigrBT53A-ZAIvZ_dIaS6llW2WcDYry15xDK759dYItLX9S0zEDiI-SEnzpNmW4zGQHK-cK0g3_13QvMHt3GRi6Gom0Iti4';
    $message = CloudMessage::withTarget('token', $deviceToken)
      ->withNotification($notification)
      ->withData($data);
    $messaging->send($message);
  }
}
