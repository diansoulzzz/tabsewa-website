<?php
namespace App\Http\Controllers\Admin\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangDetail;
use App\Models\DSewa;
use App\Models\HSewa;
use App\Models\HPiutang;
use App\Models\PengembalianDeposit;
use App\Models\Carousel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdminTransaksiDanaController extends Controller
{
  public function index(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $carousel = Carousel::find($id);
      return view('admin.menus.transaksi.dana.entry',compact('carousel'));
    }
    else {
      return view('admin.menus.transaksi.dana.entry');
    }
  }
  public function indexList(Request $request)
  {
    return view('admin.menus.transaksi.dana.list');
  }
  public function post(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'nama' => 'required_without:id|string|min:3|max:255',Rule::unique('carousel')->ignore($request->input('id')),
        'foto_url' => 'image|required_without:id',
    ]);
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->insert($request);
      }
      return $this->update($request);
    }
  }
  public function insert(Request $request)
  {
    $carousel = new Carousel();
    $carousel->nama = $request->input('nama');
    $carousel->keterangan = $request->input('keterangan');
    $carousel->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      $carousel->foto_url = Storage::disk('public')->put('carousel', $request->file('foto_url'));
    }
    $carousel->save();
    return redirect($request->url())->with('message', 'Carousel '.$request->input('nama').' berhasil didaftarkan');
  }
  public function update(Request $request)
  {
    $carousel = Carousel::find($request->input('id'));
    $carousel->nama = $request->input('nama');
    $carousel->keterangan = $request->input('keterangan');
    $carousel->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      Storage::disk('public')->delete($carousel->foto_url);
      $carousel->foto_url = Storage::disk('public')->put('carousel', $request->file('foto_url'));
    }
    $carousel->save();
    return redirect(route('admin.list.carousel'))->with('message', 'Carousel '.$request->input('nama').' berhasil didaftarkan');
  }
  public function delete(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $carousel = Carousel::find($id);
    $nama = $carousel->nama;
    $carousel->delete();
    return redirect(route('admin.list.carousel'))->with('message', 'Carousel '.$nama.' berhasil dihapus');
  }

  public function otomatis(Request $request){
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $hsewa = HSewa::with(['user_peminjam.bank_utama.bank','user_penyewa.bank_utama.bank'])->find($id);

      // $dsewas = DSewa::where('h_sewa_id',$hsewa->id)->get();
      // foreach ($dsewas as $key => $dsewa) {
      //   BarangDetail::where('id',$dsewa->barang_detail_id)->update(['status_barang'=>1]);
      // }
      // $total_stok = $dsewas->count();
      // return $hsewa;
      // $dsewa_first = DSewa::where('h_sewa_id',$hsewa->id)->first();
      // $barang_detail_first = BarangDetail::where('id',$dsewa_first->barang_detail_id)->first();
      // $barang_id = $barang_detail_first->barang_id;
      // $barang = Barang::find($barang_id);
      // $barang->stok_available = $barang->stok_available+$total_stok;
      // $barang->stok_onbook = $barang->stok_onbook-$total_stok;
      // $barang->save();
      DB::transaction(function () use ($request,$hsewa) {
        $date_today = Carbon::today();
        $total_kembali = $hsewa->grandtotal-$hsewa->deposit;
  
        if ($hsewa->status_transaksi == '-2') {
          $lp_mem = HPiutang::where('tgl','=',$date_today)->count()+1;
          $kp_mem = $this->generateCode('CDP',$date_today,$lp_mem);
          $p_mem = new HPiutang();
          $p_mem->kodenota = $kp_mem;
          $p_mem->tgl = $date_today;
          $p_mem->total_bayar = $hsewa->deposit;
          $p_mem->payment_method = 'cair_dana';
          $p_mem->status_pembayaran = 2; //2 adalah deposit
          $p_mem->users_id = $hsewa->users_peminjam_id;
          $p_mem->h_sewa_id = $hsewa->id;
          $p_mem->save();
  
          $lp_dp = new PengembalianDeposit();
          $lp_dp->tgl = $date_today;
          $lp_dp->total_pengembalian = $hsewa->deposit;
          $lp_dp->h_sewa_id = $hsewa->id;
          $lp_dp->keterangan = 'PENGEMBALIAN DANA';
          $lp_dp->save();
          // return 'Transaksi ('.$hsewa->kodenota.'). Deposit anda telah ditransfer ke rekening '.$hsewa->user_peminjam->bank_utama->nomor.' melalui bank '.$hsewa->user_peminjam->bank_utama->bank->nama;
          $this->sendMessageFromAdmin('Transaksi ('.$hsewa->kodenota.') ditolak vendor. Deposit anda telah ditransfer ke rekening ('.$hsewa->user_peminjam->bank_utama->nomor.') a.n ('.$hsewa->user_penyewa->bank_utama->nama.') melalui bank '.$hsewa->user_peminjam->bank_utama->bank->nama,'claim',Auth::id(),$hsewa->users_peminjam_id);
          return;
        }
        if ($hsewa->tgl_input_pengembalian->format('ymd') <= $hsewa->tgl_diterima_penyewa_kembali->format('ymd')) {
          $lp_mem = HPiutang::where('tgl','=',$date_today)->count()+1;
          $kp_mem = $this->generateCode('CDP',$date_today,$lp_mem);
          $p_mem = new HPiutang();
          $p_mem->kodenota = $kp_mem;
          $p_mem->tgl = $date_today;
          $p_mem->total_bayar = $hsewa->deposit;
          $p_mem->payment_method = 'cair_dana';
          $p_mem->status_pembayaran = 2; //2 adalah deposit
          $p_mem->users_id = $hsewa->users_peminjam_id;
          $p_mem->h_sewa_id = $hsewa->id;
          $p_mem->save();
  
          $lp_dp = new PengembalianDeposit();
          $lp_dp->tgl = $date_today;
          $lp_dp->total_pengembalian = $hsewa->deposit;
          $lp_dp->h_sewa_id = $hsewa->id;
          $lp_dp->keterangan = 'PENGEMBALIAN DANA';
          $lp_dp->save();
          // return 'Transaksi ('.$hsewa->kodenota.'). Deposit anda telah ditransfer ke rekening '.$hsewa->user_peminjam->bank_utama->nomor.' melalui bank '.$hsewa->user_peminjam->bank_utama->bank->nama;
          $this->sendMessageFromAdmin('Transaksi ('.$hsewa->kodenota.'). Deposit anda telah ditransfer ke rekening ('.$hsewa->user_peminjam->bank_utama->nomor.') a.n ('.$hsewa->user_penyewa->bank_utama->nama.') melalui bank '.$hsewa->user_peminjam->bank_utama->bank->nama,'claim',Auth::id(),$hsewa->users_peminjam_id);
        } else {
          $total_kembali = $total_kembali + $hsewa->deposit;
          $this->sendMessageFromAdmin('Transaksi ('.$hsewa->kodenota.'). Deposit anda telah hangus karena terlambat','claim',Auth::id(),$hsewa->users_peminjam_id);
        }
        if ($hsewa->status_transaksi <>-2){
          $lp_ven = HPiutang::where('tgl','=',$date_today)->count()+1;
          $kp_ven = $this->generateCode('CDP',$date_today,$lp_ven);
          $p_ven = new HPiutang();
          $p_ven->kodenota = $kp_ven;
          $p_ven->tgl = $date_today;
          $p_ven->total_bayar = $total_kembali;
          $p_ven->payment_method = 'cair_dana';
          $p_ven->status_pembayaran = 3; //2 adalah dana
          $p_ven->users_id = $hsewa->users_penyewa_id;
          $p_ven->h_sewa_id = $hsewa->id;
          $p_ven->save();
  
          $hsewa->status_transaksi = 8;
          $hsewa->save();
          $this->sendMessageFromAdmin('Transaksi atas '.$hsewa->kodenota.' telah selesai dan dana anda telah ditransfer ke rekening anda ('.$hsewa->user_penyewa->bank_utama->nomor.') a.n ('.$hsewa->user_penyewa->bank_utama->nama.') melalui bank '.$hsewa->user_penyewa->bank_utama->bank->nama,'claim',Auth::id(),$hsewa->users_penyewa_id);
        }
        
      }, 5);
    }
    return redirect(route('admin.list.dana'))->with('message', 'Dana berhasil diproses');
  }

  public function dataTable()
  {
    $field = ['h_sewa.id','v.nama as vendor','m.nama as member',DB::raw('h_sewa.grandtotal-h_sewa.deposit as dana_vendor'),'h_sewa.deposit as deposit','h_sewa.grandtotal','h_sewa.kodenota','h_sewa.status_transaksi'];
    $model = HSewa::doesntHave('pengembalian_deposits')->join('users as m','m.id','=','h_sewa.users_peminjam_id')
    ->join('users as v','v.id','=','h_sewa.users_penyewa_id')
    ->select($field)->whereIn('status_transaksi', ['7', '-2']);//->where('status_transaksi','6');
    $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        return '<td class="uk-text-center">
            <a href="'.route('admin.otomatis.dana', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">account_balance_wallet</i></a>
        </td>';
        // <a class="try-delete" href="'.route('admin.hapus.carousel', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      })
      ->editColumn('foto_url', function ($item) {
        return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
      })
      ->editColumn('status_transaksi', function ($item) {
        if ($item->status_transaksi == '-2'){
          return 'Ditolak Vendor';
        }
        return 'Barang vendor sudah dikembalikan';
      })
      ->escapeColumns([])
      ->make();
    return $datatable;
  }
}
