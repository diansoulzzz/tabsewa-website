<?php
namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\HSewa;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use App\Models\Bank;


class AdminDashboardController extends Controller
{
  public function index(Request $request)
  {
    $barang = Barang::get();
    $users = User::get();
    $hsewa = HSewa::get();
    $hsewa_outstanding = HSewa::doesntHave('pengembalian_deposits')->whereIn('status_transaksi', ['7', '-2'])->get();
    // return $hsewa->where('terbayar','<>','0')->sum('sisabayar');
    $data = (object)[
      'barang_count' => $barang->count(),
      'verified_count' => $users->where('verified', '!=', NULL)->count(),
      'member_count' => $users->count(),
      'vendor_count' => $users->where('is_vendor',1)->count(),
      'outstanding_piutang' => $hsewa->where('terbayar','<>','0')->count(),
      'total_piutang' => abs($hsewa->where('terbayar','<>','0')->sum('sisabayar')),
      'outstanding_kembali_dana' => $hsewa_outstanding->count(),
      'total_kembali_dana' => abs($hsewa_outstanding->sum('grandtotal')),
    ];
    // return response()->json($data);
    return view('admin.menus.dashboard.content',compact('data'));
  }
}
