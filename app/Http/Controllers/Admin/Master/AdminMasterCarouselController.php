<?php
namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Carousel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use DataTables;

class AdminMasterCarouselController extends Controller
{
  public function index(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $carousel = Carousel::find($id);
      return view('admin.menus.master.carousel.entry',compact('carousel'));
    }
    else {
      return view('admin.menus.master.carousel.entry');
    }
  }
  public function indexList(Request $request)
  {
    // return $this->dataTable();
    return view('admin.menus.master.carousel.list');
  }
  public function post(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'nama' => 'required_without:id|string|min:3|max:255',Rule::unique('carousel')->ignore($request->input('id')),
        'foto_url' => 'image|required_without:id',
    ]);
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->insert($request);
      }
      return $this->update($request);
    }
  }
  public function insert(Request $request)
  {
    $carousel = new Carousel();
    $carousel->nama = $request->input('nama');
    $carousel->keterangan = $request->input('keterangan');
    $carousel->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      $carousel->foto_url = Storage::disk('public')->put('carousel', $request->file('foto_url'));
    }
    $carousel->save();
    return redirect($request->url())->with('message', 'Carousel '.$request->input('nama').' berhasil didaftarkan');
  }
  public function update(Request $request)
  {
    $carousel = Carousel::find($request->input('id'));
    $carousel->nama = $request->input('nama');
    $carousel->keterangan = $request->input('keterangan');
    $carousel->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      Storage::disk('public')->delete($carousel->foto_url);
      $carousel->foto_url = Storage::disk('public')->put('carousel', $request->file('foto_url'));
    }
    $carousel->save();
    return redirect(route('admin.list.carousel'))->with('message', 'Carousel '.$request->input('nama').' berhasil didaftarkan');
  }
  public function delete(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $carousel = Carousel::find($id);
    $nama = $carousel->nama;
    $carousel->delete();
    return redirect(route('admin.list.carousel'))->with('message', 'Carousel '.$nama.' berhasil dihapus');
  }
  public function dataTable()
  {
    $field = ['id','nama','foto_url'];
    $model = Carousel::select($field);
    $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        return '<td class="uk-text-center">
            <a href="'.route('admin.input.carousel', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
            <a class="try-delete" href="'.route('admin.hapus.carousel', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
        </td>';
      })
      ->editColumn('foto_url', function ($item) {
        return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
      })
      ->escapeColumns([])
      ->make();
    return $datatable;
  }
}
