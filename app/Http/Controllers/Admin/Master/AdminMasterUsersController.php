<?php
namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use DataTables;

class AdminMasterUsersController extends Controller
{
  public function index(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $users = User::find($id);
      return view('admin.menus.master.users.entry',compact('users'));
    }
    else {
      return view('admin.menus.master.users.entry');
    }
  }
  public function indexList(Request $request)
  {
    // return User::get();
    return view('admin.menus.master.users.list');
  }
  public function delete(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $users = User::find($id);
    $nama = $users->nama;
    $message = 'unblock';
    if ($users->aktif){
      $message = 'block';
    }
    $users->aktif = !$users->aktif;
    $users->save();
    return redirect(route('admin.list.users'))->with('message', 'User '.$nama.' berhasil di '.$message);
  }
  public function dataTable()
  {
    $field = ['id','nama','foto_url','aktif','verified'];
    $model = User::select($field);
    $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        $icons = 'restore';
        $status = 'unblock';
        if ($item->aktif){
          $icons = 'block';
          $status = 'block';
        }
        return '<td class="uk-text-center">
            <a class="try-delete" status="'.$status.'" href="'.route('admin.hapus.users', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">'.$icons.'</i></a>
        </td>';
      })
      ->editColumn('foto_url', function ($item) {
        return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
      })
      ->editColumn('verified', function ($item) {
        if ($item->verified) {
          return '<td class="uk-text-center"><i class="md-icon material-icons">verified_user</i></td>';
        }
      })
      ->escapeColumns([])
      ->make();
    return $datatable;
  }
}
