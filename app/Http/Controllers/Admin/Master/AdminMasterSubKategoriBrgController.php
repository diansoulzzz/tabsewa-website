<?php
namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use DataTables;

class AdminMasterSubKategoriBrgController extends Controller
{
  public function index(Request $request)
  {
    $kategori = BarangKategori::get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $subkategori = BarangSubKategori::find($id);
      return view('admin.menus.master.subkategori.entry',compact('subkategori','kategori'));
    }
    else {
      return view('admin.menus.master.subkategori.entry',compact('kategori'));
    }
  }
  public function indexList(Request $request)
  {
    return view('admin.menus.master.subkategori.list');
  }
  public function post(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'nama' => 'required_without:id|string|max:255',Rule::unique('barang_sub_kategori')->ignore($request->input('id')),
        // 'foto_url' => 'image|required_without:id',
        'barang_kategori_id' => 'required|exists:barang_kategori,id',
        // unique:barang_kategori,nama,'. (isset($data['id']) ? $data['id']: '')
    ]);
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->insert($request);
      }
      return $this->update($request);
    }
  }
  public function insert(Request $request)
  {
    $subkategori = new BarangSubKategori();
    $subkategori->nama = $request->input('nama');
    $subkategori->barang_kategori_id = $request->input('barang_kategori_id');
    $subkategori->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      $subkategori->foto_url = Storage::disk('public')->put('subkategori', $request->file('foto_url'));
    }
    $subkategori->save();
    return redirect($request->url())->with('message', 'SubKategori '.$request->input('nama').' berhasil didaftarkan');
  }
  public function update(Request $request)
  {
    $subkategori = BarangSubKategori::find($request->input('id'));
    $subkategori->nama = $request->input('nama');
    $subkategori->barang_kategori_id = $request->input('barang_kategori_id');
    $subkategori->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      Storage::disk('public')->delete($subkategori->foto_url);
      $subkategori->foto_url = Storage::disk('public')->put('subkategori', $request->file('foto_url'));
    }
    $subkategori->save();
    return redirect(route('admin.list.subkategori'))->with('message', 'SubKategori '.$request->input('nama').' berhasil didaftarkan');
  }
  public function delete(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $subkategori = BarangSubKategori::find($id);
    $nama = $subkategori->nama;
    $subkategori->delete();
    return redirect(route('admin.list.subkategori'))->with('message', 'Kategori '.$nama.' berhasil dihapus');
  }
  public function dataTable()
  {
    $field = ['id','nama','foto_url','barang_kategori_id'];
    $model = BarangSubKategori::with(['barang_kategori'=>function ($query){
      $query->addSelect(['id','nama']);
    }])->select($field);
    $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        return '<td class="uk-text-center">
            <a href="'.route('admin.input.subkategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
            <a class="try-delete" href="'.route('admin.hapus.subkategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
        </td>';
      })
      ->editColumn('foto_url', function ($item) {
        return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
      })
      ->editColumn('barang_kategori', function ($item) {
        return '<td class="uk-text-center">'.$item->barang_kategori->nama.'</td>';
      })
      ->escapeColumns([])
      ->make();
    return $datatable;
  }
}
