<?php
namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use DataTables;

class AdminMasterKategoriBrgController extends Controller
{
  public function index(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $kategori = BarangKategori::find($id);
      return view('admin.menus.master.kategori.entry',compact('kategori'));
    }
    else {
      return view('admin.menus.master.kategori.entry');
    }
  }
  public function indexList(Request $request)
  {
    // return $this->dataTable();
    return view('admin.menus.master.kategori.list');
  }
  public function post(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'nama' => 'required_without:id|string|min:3|max:255',Rule::unique('barang_kategori')->ignore($request->input('id')),
        'foto_url' => 'image|required_without:id',
        // unique:barang_kategori,nama,'. (isset($data['id']) ? $data['id']: '')
    ]);
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->insert($request);
      }
      return $this->update($request);
    }
  }
  public function insert(Request $request)
  {
    $kategori = new BarangKategori();
    $kategori->nama = $request->input('nama');
    $kategori->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      $kategori->foto_url = Storage::disk('public')->put('kategori', $request->file('foto_url'));
    }
    $kategori->save();
    return redirect($request->url())->with('message', 'Kategori '.$request->input('nama').' berhasil didaftarkan');
  }
  public function update(Request $request)
  {
    $kategori = BarangKategori::find($request->input('id'));
    $kategori->nama = $request->input('nama');
    $kategori->users_id = Auth::id();
    if ($request->hasFile('foto_url')) {
      Storage::disk('public')->delete($kategori->foto_url);
      $kategori->foto_url = Storage::disk('public')->put('kategori', $request->file('foto_url'));
    }
    $kategori->save();
    return redirect(route('admin.list.kategori'))->with('message', 'Kategori '.$request->input('nama').' berhasil didaftarkan');
  }
  public function delete(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $kategori = BarangKategori::find($id);
    $nama = $kategori->nama;
    $kategori->delete();
    return redirect(route('admin.list.kategori'))->with('message', 'Kategori '.$nama.' berhasil dihapus');
  }
  public function dataTable()
  {
    $field = ['id','nama','foto_url'];
    $model = BarangKategori::select($field);
    $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        return '<td class="uk-text-center">
            <a href="'.route('admin.input.kategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
            <a class="try-delete" href="'.route('admin.hapus.kategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
        </td>';
      })
      ->editColumn('foto_url', function ($item) {
        return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
      })
      ->escapeColumns([])
      ->make();
    return $datatable;
  }
}
