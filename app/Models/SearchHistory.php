<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SearchHistory
 * 
 * @property int $id
 * @property string $query
 * @property int $users_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class SearchHistory extends Eloquent
{
	protected $table = 'search_history';
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int'
	];

	protected $fillable = [
		'query',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
