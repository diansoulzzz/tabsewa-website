<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HPiutang
 *
 * @property int $id
 * @property string $kodenota
 * @property \Carbon\Carbon $tgl
 * @property float $total_bayar
 * @property string $payment_method
 * @property int $status_pembayaran
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 * @property int $h_sewa_id
 * @property int $users_peminjam_bank_id
 * @property string $midtrans_signature_key
 * @property string $midtrans_transaction_id
 * @property string $midtrans_approval_code
 * @property string $midtrans_transaction_status
 * @property string $midtrans_payment_code
 * @property string $midtrans_store
 * @property string $midtrans_status_message
 * @property string $midtrans_status_code
 *
 * @property \App\Models\HSewa $h_sewa
 * @property \App\Models\User $user
 * @property \App\Models\UsersBank $users_bank
 *
 * @package App\Models
 */
class HPiutang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_piutang';

	protected $casts = [
		'total_bayar' => 'float',
		'status_pembayaran' => 'int',
		'users_id' => 'int',
		'h_sewa_id' => 'int',
		'users_peminjam_bank_id' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'kodenota',
		'tgl',
		'total_bayar',
		'payment_method',
		'status_pembayaran',
		'users_id',
		'h_sewa_id',
		'users_peminjam_bank_id',
		'midtrans_signature_key',
		'midtrans_transaction_id',
		'midtrans_approval_code',
		'midtrans_transaction_status',
		'midtrans_payment_code',
		'midtrans_store',
		'midtrans_status_message',
		'midtrans_status_code'
	];

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function users_bank()
	{
		return $this->belongsTo(\App\Models\UsersBank::class, 'users_peminjam_bank_id');
	}
}
