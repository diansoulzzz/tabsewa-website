<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HSewa
 *
 * @property int $id
 * @property string $kodenota
 * @property \Carbon\Carbon $tgl
 * @property float $deposit
 * @property float $biaya_ongkir
 * @property float $grandtotal
 * @property float $sisabayar
 * @property float $terbayar
 * @property \Carbon\Carbon $tgl_input_peminjaman
 * @property \Carbon\Carbon $tgl_input_pengembalian
 * @property \Carbon\Carbon $tgl_konfirmasi_penyewa
 * @property \Carbon\Carbon $tgl_dikirim_penyewa
 * @property \Carbon\Carbon $tgl_diterima_peminjam
 * @property \Carbon\Carbon $tgl_diterima_penyewa_kembali
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_peminjam_id
 * @property int $users_penyewa_id
 * @property int $status_transaksi
 * @property string $midtrans_order_id
 *
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $d_sewas
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $pengembalian_deposits
 * @property \Illuminate\Database\Eloquent\Collection $review_barangs
 *
 * @package App\Models
 */
class HSewa extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_sewa';

	protected $casts = [
		'deposit' => 'float',
		'biaya_ongkir' => 'float',
		'grandtotal' => 'float',
		'sisabayar' => 'float',
		'terbayar' => 'float',
		'users_peminjam_id' => 'int',
		'users_penyewa_id' => 'int',
		'status_transaksi' => 'int'
	];

	protected $dates = [
		'tgl',
		'tgl_input_peminjaman',
		'tgl_input_pengembalian',
		'tgl_konfirmasi_penyewa',
		'tgl_dikirim_penyewa',
		'tgl_diterima_peminjam',
		'tgl_diterima_penyewa_kembali'
	];

	protected $fillable = [
		'kodenota',
		'tgl',
		'deposit',
		'biaya_ongkir',
		'grandtotal',
		'sisabayar',
		'terbayar',
		'tgl_input_peminjaman',
		'tgl_input_pengembalian',
		'tgl_konfirmasi_penyewa',
		'tgl_dikirim_penyewa',
		'tgl_diterima_peminjam',
		'tgl_diterima_penyewa_kembali',
		'users_peminjam_id',
		'users_penyewa_id',
		'status_transaksi',
		'midtrans_order_id'
	];

	public function user_peminjam()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_peminjam_id');
	}

	public function user_penyewa()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_penyewa_id');
	}

	public function d_sewas()
	{
		return $this->hasMany(\App\Models\DSewa::class);
	}

	public function d_sewa_utama()
	{
		return $this->hasOne(\App\Models\DSewa::class);
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class);
	}

	public function pengembalian_deposits()
	{
		return $this->hasMany(\App\Models\PengembalianDeposit::class);
	}

	public function review_barangs()
	{
		return $this->hasMany(\App\Models\ReviewBarang::class);
	}

	public function review_barang_utama()
	{
		return $this->hasOne(\App\Models\ReviewBarang::class);
	}

	public function h_piutang_utama()
	{
		return $this->hasOne(\App\Models\HPiutang::class);
	}
}
