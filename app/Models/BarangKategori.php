<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class BarangKategori
 *
 * @property int $id
 * @property string $nama
 * @property string $foto_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 *
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $barang_sub_kategoris
 *
 * @package App\Models
 */
class BarangKategori extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang_kategori';

	protected $casts = [
		'users_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'foto_url',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function barang_sub_kategoris()
	{
		return $this->hasMany(\App\Models\BarangSubKategori::class);
	}

	public function getFotoUrlAttribute($value)
  {
		if (filter_var($value, FILTER_VALIDATE_URL)) {
			return $value;
		}
		return Storage::disk('public')->url($value);
  }
}
