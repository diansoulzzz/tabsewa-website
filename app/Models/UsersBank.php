<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersBank
 * 
 * @property int $id
 * @property string $nomor
 * @property string $nama
 * @property int $bank_id
 * @property int $users_id
 * @property int $utama
 * 
 * @property \App\Models\Bank $bank
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 *
 * @package App\Models
 */
class UsersBank extends Eloquent
{
	protected $table = 'users_bank';
	public $timestamps = false;

	protected $casts = [
		'bank_id' => 'int',
		'users_id' => 'int',
		'utama' => 'int'
	];

	protected $fillable = [
		'nomor',
		'nama',
		'bank_id',
		'users_id',
		'utama'
	];

	public function bank()
	{
		return $this->belongsTo(\App\Models\Bank::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class, 'users_peminjam_bank_id');
	}
}
