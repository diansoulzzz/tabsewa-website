<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class BarangSubKategori
 *
 * @property int $id
 * @property string $nama
 * @property string $foto_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $barang_kategori_id
 * @property int $users_id
 *
 * @property \App\Models\BarangKategori $barang_kategori
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 *
 * @package App\Models
 */
class BarangSubKategori extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang_sub_kategori';

	protected $casts = [
		'barang_kategori_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'foto_url',
		'barang_kategori_id',
		'users_id'
	];

	public function barang_kategori()
	{
		return $this->belongsTo(\App\Models\BarangKategori::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function barangs()
	{
		return $this->hasMany(\App\Models\Barang::class);
	}

	public function getFotoUrlAttribute($value)
  {
		if (filter_var($value, FILTER_VALIDATE_URL)) {
			return $value;
		}
		return Storage::disk('public')->url($value);
  }
}
