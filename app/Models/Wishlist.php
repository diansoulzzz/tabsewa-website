<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Apr 2019 08:16:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Wishlist
 * 
 * @property int $id
 * @property int $users_id
 * @property int $barang_id
 * @property \Carbon\Carbon $created_at
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Wishlist extends Eloquent
{
	protected $table = 'wishlist';
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'barang_id' => 'int'
	];

	protected $fillable = [
		'users_id',
		'barang_id'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
