<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DSewa
 *
 * @property int $id
 * @property int $barang_detail_id
 * @property int $h_sewa_id
 * @property \Carbon\Carbon $tgl_input_sewa_awal
 * @property \Carbon\Carbon $tgl_input_sewa_akhir
 * @property int $lama_sewa
 * @property float $harga_sewa
 * @property float $subtotal
 *
 * @property \App\Models\BarangDetail $barang_detail
 * @property \App\Models\HSewa $h_sewa
 *
 * @package App\Models
 */
class DSewa extends Eloquent
{
	protected $table = 'd_sewa';
	public $timestamps = false;

	protected $casts = [
		'barang_detail_id' => 'int',
		'h_sewa_id' => 'int',
		'lama_sewa' => 'int',
		'harga_sewa' => 'float',
		'subtotal' => 'float'
	];

	protected $dates = [
		'tgl_input_sewa_awal',
		'tgl_input_sewa_akhir'
	];

	protected $fillable = [
		'barang_detail_id',
		'h_sewa_id',
		'tgl_input_sewa_awal',
		'tgl_input_sewa_akhir',
		'lama_sewa',
		'harga_sewa',
		'subtotal'
	];

	public function barang_detail()
	{
		return $this->belongsTo(\App\Models\BarangDetail::class);
	}

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}
}
