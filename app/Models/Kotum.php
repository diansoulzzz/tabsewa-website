<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Kotum
 * 
 * @property int $id
 * @property string $nama
 * @property string $tipe
 * @property string $kode_pos
 * @property int $provinsi_id
 * 
 * @property \App\Models\Provinsi $provinsi
 * @property \Illuminate\Database\Eloquent\Collection $kecamatans
 *
 * @package App\Models
 */
class Kotum extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'provinsi_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'tipe',
		'kode_pos',
		'provinsi_id'
	];

	public function provinsi()
	{
		return $this->belongsTo(\App\Models\Provinsi::class);
	}

	public function kecamatans()
	{
		return $this->hasMany(\App\Models\Kecamatan::class, 'kota_id');
	}
}
