<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $nama
 * @property string $code
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
	protected $table = 'bank';
	public $timestamps = false;

	protected $fillable = [
		'nama',
		'code'
	];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_bank', 'bank_id', 'users_id')
					->withPivot('id', 'nomor', 'nama', 'utama');
	}
}
