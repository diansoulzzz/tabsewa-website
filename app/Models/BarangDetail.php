<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangDetail
 * 
 * @property int $id
 * @property int $barang_id
 * @property int $status_barang
 * 
 * @property \App\Models\Barang $barang
 * @property \Illuminate\Database\Eloquent\Collection $d_sewas
 *
 * @package App\Models
 */
class BarangDetail extends Eloquent
{
	protected $table = 'barang_detail';
	public $timestamps = false;

	protected $casts = [
		'barang_id' => 'int',
		'status_barang' => 'int'
	];

	protected $fillable = [
		'barang_id',
		'status_barang'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function d_sewas()
	{
		return $this->hasMany(\App\Models\DSewa::class);
	}
}
