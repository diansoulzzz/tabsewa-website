<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Provinsi
 * 
 * @property int $id
 * @property string $nama
 * 
 * @property \Illuminate\Database\Eloquent\Collection $kota
 *
 * @package App\Models
 */
class Provinsi extends Eloquent
{
	protected $table = 'provinsi';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];

	public function kota()
	{
		return $this->hasMany(\App\Models\Kotum::class);
	}
}
