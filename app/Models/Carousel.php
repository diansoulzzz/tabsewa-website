<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class Carousel
 *
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 * @property \Carbon\Carbon $tgl_awal
 * @property \Carbon\Carbon $tgl_akhir
 * @property string $foto_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Carousel extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'carousel';

	protected $casts = [
		'users_id' => 'int'
	];

	protected $dates = [
		'tgl_awal',
		'tgl_akhir'
	];

	protected $fillable = [
		'nama',
		'keterangan',
		'tgl_awal',
		'tgl_akhir',
		'foto_url',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function getFotoUrlAttribute($value)
  {
		if (filter_var($value, FILTER_VALIDATE_URL)) {
			return $value;
		}
		return Storage::disk('public')->url($value);
  }
}
