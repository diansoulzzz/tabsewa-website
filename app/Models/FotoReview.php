<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FotoReview
 * 
 * @property int $id
 * @property string $foto_url
 * @property int $review_barang_id
 * 
 * @property \App\Models\ReviewBarang $review_barang
 *
 * @package App\Models
 */
class FotoReview extends Eloquent
{
	protected $table = 'foto_review';
	public $timestamps = false;

	protected $casts = [
		'review_barang_id' => 'int'
	];

	protected $fillable = [
		'foto_url',
		'review_barang_id'
	];

	public function review_barang()
	{
		return $this->belongsTo(\App\Models\ReviewBarang::class);
	}
}
