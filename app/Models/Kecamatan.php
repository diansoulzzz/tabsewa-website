<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Kecamatan
 *
 * @property int $id
 * @property string $nama
 * @property int $kota_id
 *
 * @property \App\Models\Kotum $kotum
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Kecamatan extends Eloquent
{
	protected $table = 'kecamatan';
	public $timestamps = false;

	protected $casts = [
		'kota_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'kota_id'
	];

	public function kotum()
	{
		return $this->belongsTo(\App\Models\Kotum::class, 'kota_id');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}
}
