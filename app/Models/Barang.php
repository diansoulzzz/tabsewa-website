<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

/**
 * Class Barang
 *
 * @property int $id
 * @property string $nama
 * @property string $deskripsi
 * @property int $barang_sub_kategori_id
 * @property int $users_id
 * @property int $stok
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property float $deposit_min
 * @property int $stok_available
 * @property int $stok_onbook
 * @property float $ongkir_dalam_jawa
 * @property float $ongkir_luar_jawa
 *
 * @property \App\Models\BarangSubKategori $barang_sub_kategori
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $barang_details
 * @property \Illuminate\Database\Eloquent\Collection $barang_fotos
 * @property \Illuminate\Database\Eloquent\Collection $barang_harga_sewas
 *
 * @package App\Models
 */
class Barang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang';

	protected $casts = [
		'barang_sub_kategori_id' => 'int',
		'users_id' => 'int',
		'stok' => 'int',
		'deposit_min' => 'float',
		'stok_available' => 'int',
		'stok_onbook' => 'int',
		'ongkir_dalam_jawa' => 'float',
		'ongkir_luar_jawa' => 'float'
	];

	protected $fillable = [
		'nama',
		'deskripsi',
		'barang_sub_kategori_id',
		'users_id',
		'stok',
		'deposit_min',
		'stok_available',
		'stok_onbook',
		'ongkir_dalam_jawa',
		'ongkir_luar_jawa'
	];

	protected $appends = ['total_rating'];

	public function barang_sub_kategori()
	{
		return $this->belongsTo(\App\Models\BarangSubKategori::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function barang_details()
	{
		return $this->hasMany(\App\Models\BarangDetail::class);
	}

	public function barang_fotos()
	{
		return $this->hasMany(\App\Models\BarangFoto::class)->orderBy('utama', 'desc');
	}

	public function barang_harga_sewas()
	{
		return $this->hasMany(\App\Models\BarangHargaSewa::class);
	}

	public function barang_foto_utama()
	{
		return $this->hasOne(\App\Models\BarangFoto::class)->where('utama', 1);
	}

	public function barang_harga_sewa_utama()
	{
		return $this->hasOne(\App\Models\BarangHargaSewa::class)->where('lama_hari', 1);
	}

	public function barang_review()
	{
		return $this->hasMany(\App\Models\ReviewBarang::class, 'barang_id');
	}

	public function wishlists()
	{
		return $this->hasMany(\App\Models\Wishlist::class, 'barang_id');
	}

	public function last_seen_barangs()
	{
		return $this->hasMany(\App\Models\LastSeenBarang::class, 'barang_id');
	}

	public function wishlisted()
	{
		return $this->hasOne(\App\Models\Wishlist::class, 'barang_id')->where('users_id', Auth::id());
	}

	public function getTotalRatingAttribute()
	{
		if ($this->barang_review()->exists()) {
			return $this->barang_review()->avg('rating');
		}
		return 0;
	}
}
