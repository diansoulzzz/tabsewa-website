<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PengembalianDeposit
 * 
 * @property int $id
 * @property \Carbon\Carbon $tgl
 * @property float $total_pengembalian
 * @property int $h_sewa_id
 * @property string $keterangan
 * 
 * @property \App\Models\HSewa $h_sewa
 *
 * @package App\Models
 */
class PengembalianDeposit extends Eloquent
{
	protected $table = 'pengembalian_deposit';
	public $timestamps = false;

	protected $casts = [
		'total_pengembalian' => 'float',
		'h_sewa_id' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'tgl',
		'total_pengembalian',
		'h_sewa_id',
		'keterangan'
	];

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}
}
