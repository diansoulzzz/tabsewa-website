<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 28 Feb 2019 03:26:56 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

/**
 * Class Message
 *
 * @property int $id
 * @property string $value
 * @property string $type
 * @property int $is_read
 * @property string $uuid
 * @property int $users_id_from
 * @property int $users_id_to
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Message extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'message';
	protected $appends = ['is_sender'];
	protected $casts = [
		'is_read' => 'int',
		'users_id_from' => 'int',
		'users_id_to' => 'int',
		'is_sender' => 'int'
	];

	protected $fillable = [
		'value',
		'type',
		'is_read',
		'uuid',
		'users_id_from',
		'users_id_to'
	];

	public function user_from()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id_from');
	}

	public function user_to()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id_to');
	}
	public function getIsSenderAttribute(){
		if (Auth::id() == $this->users_id_from) {
			return 1;
		}
		return 0;
	}
}
