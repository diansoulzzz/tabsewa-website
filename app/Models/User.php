<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 14:11:58 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

/**
 * Class User
 *
 * @property int $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $foto_url
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $aktif
 * @property string $firebase_auth_provider_id
 * @property string $api_token
 * @property \Carbon\Carbon $verified
 * @property string $no_ktp
 * @property string $foto_ktp_url
 * @property string $firebase_auth_token
 * @property string $firebase_auth_uid
 * @property string $firebase_instance_id
 * @property string $firebase_fcm_token
 * @property string $no_telp
 * @property string $firebase_auth_password_hash
 * @property \Carbon\Carbon $firebase_auth_token_valid_at
 * @property \Carbon\Carbon $tgl_lahir
 * @property \Carbon\Carbon $firebase_auth_last_login_at
 * @property int $is_vendor
 * @property string $alamat
 * @property int $is_jawa
 * @property int $kecamatan_id
 *
 * @property \App\Models\Kecamatan $kecamatan
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 * @property \Illuminate\Database\Eloquent\Collection $barang_kategoris
 * @property \Illuminate\Database\Eloquent\Collection $barang_sub_kategoris
 * @property \Illuminate\Database\Eloquent\Collection $carousels
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $h_sewas
 * @property \Illuminate\Database\Eloquent\Collection $review_barangs
 * @property \Illuminate\Database\Eloquent\Collection $search_histories
 * @property \Illuminate\Database\Eloquent\Collection $banks
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	protected $casts = [
		'aktif' => 'int',
		'is_vendor' => 'int',
		'is_jawa' => 'int',
		'kecamatan_id' => 'int'
	];

	protected $dates = [
		'verified',
		'firebase_auth_token_valid_at',
		'tgl_lahir',
		'firebase_auth_last_login_at',
		'approved_at',
	];

	protected $hidden = [
		'password',
		'remember_token',
		'api_token',
		'firebase_auth_token',
		'firebase_fcm_token',
		'h_piutangs',
	];

	protected $fillable = [
		'nama',
		'email',
		'password',
		'foto_url',
		'remember_token',
		'aktif',
		'firebase_auth_provider_id',
		'api_token',
		'verified',
		'no_ktp',
		'foto_ktp_url',
		'firebase_auth_token',
		'firebase_auth_uid',
		'firebase_instance_id',
		'firebase_fcm_token',
		'no_telp',
		'firebase_auth_password_hash',
		'firebase_auth_token_valid_at',
		'tgl_lahir',
		'firebase_auth_last_login_at',
		'is_vendor',
		'is_jawa',
		'kecamatan_id',
		'alamat',
		'kode_pos',
		'pin_alamat',
		'pin_latitude',
		'pin_longitude',
		'approved_at',
	];

	// protected $appends = ['total_saldo'];

	public function kecamatan()
	{
		return $this->belongsTo(\App\Models\Kecamatan::class);
	}

	public function barangs()
	{
		return $this->hasMany(\App\Models\Barang::class, 'users_id');
	}

	public function barang_kategoris()
	{
		return $this->hasMany(\App\Models\BarangKategori::class, 'users_id');
	}

	public function barang_sub_kategoris()
	{
		return $this->hasMany(\App\Models\BarangSubKategori::class, 'users_id');
	}

	public function carousels()
	{
		return $this->hasMany(\App\Models\Carousel::class, 'users_id');
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class, 'users_id');
	}

	public function h_sewas_peminjam()
	{
		return $this->hasMany(\App\Models\HSewa::class, 'users_peminjam_id');
	}

	public function h_sewas_penyewa()
	{
		return $this->hasMany(\App\Models\HSewa::class, 'users_penyewa_id');
	}

	public function review_barangs()
	{
		return $this->hasMany(\App\Models\ReviewBarang::class, 'users_id');
	}

	public function search_histories()
	{
		return $this->hasMany(\App\Models\SearchHistory::class, 'users_id');
	}

	public function messages_from()
	{
		return $this->hasMany(\App\Models\Message::class, 'users_id_from');
	}

	public function messages_to()
	{
		return $this->hasMany(\App\Models\Message::class, 'users_id_to');
	}

	public function banks()
	{
		return $this->belongsToMany(\App\Models\Bank::class, 'users_bank', 'users_id')
			->withPivot('id', 'nomor', 'nama', 'utama');
	}

	public function wishlists()
	{
		return $this->hasMany(\App\Models\Wishlist::class, 'users_id');
	}

	public function last_seen_barangs()
	{
		return $this->hasMany(\App\Models\LastSeenBarang::class, 'users_id');
	}

	public function getFotoUrlAttribute($value)
	{
		if (is_null($value)) {
			return asset('assets/admin/img/avatars/default-user.png');
		}
		if (filter_var($value, FILTER_VALIDATE_URL)) {
			return $value;
		}
		return Storage::disk('public')->url($value);
	}

	public function bank_utama()
	{
		return $this->hasOne(\App\Models\UsersBank::class, 'users_id')->where('utama', 1);
	}
	
	public function getTotalSaldoAttribute()
	{
		return $this->h_piutangs->whereIn('payment_method', ['cair_dana','tarik_dana'])->sum('total_bayar');
	}
	
	public function h_piutangs_complete()
	{
		return $this->h_piutangs()->whereIn('payment_method', ['cair_dana','tarik_dana']);
	}
}
