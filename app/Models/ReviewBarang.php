<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Apr 2019 08:17:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ReviewBarang
 * 
 * @property int $id
 * @property int $rating
 * @property string $komentar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $h_sewa_id
 * @property int $users_id
 * @property int $barang_id
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\HSewa $h_sewa
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $foto_reviews
 *
 * @package App\Models
 */
class ReviewBarang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'review_barang';

	protected $casts = [
		'rating' => 'int',
		'h_sewa_id' => 'int',
		'users_id' => 'int',
		'barang_id' => 'int'
	];

	protected $fillable = [
		'rating',
		'komentar',
		'h_sewa_id',
		'users_id',
		'barang_id'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function foto_reviews()
	{
		return $this->hasMany(\App\Models\FotoReview::class);
	}
}
