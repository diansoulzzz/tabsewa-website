C:\Windows\System32\drivers\etc\hosts SETUP :
127.0.0.1 admin-tabsewa.local.com
127.0.0.1 api-tabsewa.local.com

httpd-vhosts.conf

<VirtualHost *:80>
	DocumentRoot "E:/xampp/htdocs/tabsewa"
	ServerName admin-tabsewa.local.com
</VirtualHost>

<VirtualHost *:80>
	DocumentRoot "E:/xampp/htdocs/tabsewa"
	ServerName api-tabsewa.local.com
</VirtualHost>

php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan clear-compiled
php artisan config:cache

replace vendor/kreait/firebase-php/src/Firebase/Messaging/Notification.php :

<?php

declare(strict_types=1);

namespace Kreait\Firebase\Messaging;

use Kreait\Firebase\Exception\InvalidArgumentException;

class Notification implements \JsonSerializable
{
    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $body;
    /**
     * @var string|null
     */
    private $click_action;

    private function __construct(string $title = null, string $body = null, string $click_action = null)
    {
        $this->title = $title;
        $this->body = $body;
        $this->click_action = $click_action;
    }

    public static function create(string $title = null, string $body = null, string $click_action = null): self
    {
        return new self($title, $body, $click_action);
    }

    public static function fromArray(array $data): self
    {
        try {
            return new self(
                $data['title'] ?? null,
                $data['body'] ?? null,
                $data['click_action'] ?? null
            );
        } catch (\Throwable $e) {
            throw new InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function withTitle(string $title): self
    {
        $notification = clone $this;
        $notification->title = $title;

        return $notification;
    }

    public function withBody(string $body): self
    {
        $notification = clone $this;
        $notification->body = $body;

        return $notification;
    }

    public function withClickAction(string $click_action): self
    {
        $notification = clone $this;
        $notification->click_action = $click_action;

        return $notification;
    }

    /**
     * @return string|null
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function body()
    {
        return $this->body;
    }

    /**
     * @return string|null
     */
    public function click_action()
    {
        return $this->click_action;
    }

    public function jsonSerialize()
    {
        return array_filter([
            'title' => $this->title,
            'body' => $this->body,
            'click_action' => $this->click_action,
        ], function ($value) {
            return $value !== null;
        });
    }
}
