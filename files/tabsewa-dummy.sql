-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tabsewa
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `deskripsi` text,
  `barang_sub_kategori_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deposit_min` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_barang_sub_kategori1_idx` (`barang_sub_kategori_id`),
  KEY `fk_barang_users1_idx` (`users_id`),
  CONSTRAINT `fk_barang_barang_sub_kategori1` FOREIGN KEY (`barang_sub_kategori_id`) REFERENCES `barang_sub_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_barang_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (3,'TEST',NULL,1,1,10,'2018-01-01 00:00:00','2018-01-01 00:00:00',NULL,1000.00),(4,'HMM',NULL,2,1,100,'2018-01-01 00:00:00','2018-01-01 00:00:00',NULL,100.00),(5,'TEST CARSEAT',NULL,1,1,10,'2018-01-01 00:00:00','2018-01-01 00:00:00',NULL,1000.00);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_foto`
--

DROP TABLE IF EXISTS `barang_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_url` text,
  `utama` tinyint(4) DEFAULT NULL,
  `barang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_foto_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_barang_foto_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_foto`
--

LOCK TABLES `barang_foto` WRITE;
/*!40000 ALTER TABLE `barang_foto` DISABLE KEYS */;
INSERT INTO `barang_foto` VALUES (5,'kategori/FYFXtephYGhwh6xUsvgTWrRBfzyoJn6napWKPt1z.png',1,3),(6,'kategori/FYFXtephYGhwh6xUsvgTWrRBfzyoJn6napWKPt1z.png',0,3),(7,'kategori/FOhEAm0yvK00Y891vcxI85kpjKSqR1snOh0gELfD.png',1,4),(8,'kategori/1LcQFbOHwE5aE0v2x3DzYsjATFQNHNqroue22hB0.png',0,4),(9,'kategori/9B1pGVMkds0RllJrFeIqYNiyM1HAzcw7TBkDwsgG.png',1,5);
/*!40000 ALTER TABLE `barang_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_harga_sewa`
--

DROP TABLE IF EXISTS `barang_harga_sewa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_harga_sewa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `lama_hari` int(11) DEFAULT NULL,
  `harga` decimal(14,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_harga_sewa_barang_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_harga_sewa_barang_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_harga_sewa`
--

LOCK TABLES `barang_harga_sewa` WRITE;
/*!40000 ALTER TABLE `barang_harga_sewa` DISABLE KEYS */;
INSERT INTO `barang_harga_sewa` VALUES (1,3,1,10000.00,NULL,NULL,NULL),(2,4,1,20000.00,NULL,NULL,NULL),(3,3,7,65000.00,NULL,NULL,NULL),(4,4,7,130000.00,NULL,NULL,NULL),(5,5,1,100000.00,NULL,NULL,NULL);
/*!40000 ALTER TABLE `barang_harga_sewa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_kategori`
--

DROP TABLE IF EXISTS `barang_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `foto_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_kategori_users1_idx` (`users_id`),
  CONSTRAINT `fk_barang_kategori_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_kategori`
--

LOCK TABLES `barang_kategori` WRITE;
/*!40000 ALTER TABLE `barang_kategori` DISABLE KEYS */;
INSERT INTO `barang_kategori` VALUES (2,'Elektronik','kategori/fMgwjvrbZ2qtGqh9SVb6X01FrxSrYvnjtESZZNVd.png','2018-03-22 07:34:14','2018-04-08 05:33:20',NULL,1),(3,'Ibu & Anak','kategori/FYFXtephYGhwh6xUsvgTWrRBfzyoJn6napWKPt1z.png','2018-03-22 07:34:45','2018-04-08 05:50:26',NULL,1),(4,'Kamera & Audio','kategori/FOhEAm0yvK00Y891vcxI85kpjKSqR1snOh0gELfD.png','2018-03-22 08:19:51','2018-04-08 05:34:05',NULL,1),(5,'Tas & Koper','kategori/1LcQFbOHwE5aE0v2x3DzYsjATFQNHNqroue22hB0.png','2018-04-08 05:36:22','2018-04-08 05:36:22',NULL,1),(6,'Fashion','kategori/9B1pGVMkds0RllJrFeIqYNiyM1HAzcw7TBkDwsgG.png','2018-05-11 07:18:48','2018-05-11 07:18:48',NULL,1);
/*!40000 ALTER TABLE `barang_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_sub_kategori`
--

DROP TABLE IF EXISTS `barang_sub_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_sub_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `foto_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `barang_kategori_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_sub_kategori_barang_kategori1_idx` (`barang_kategori_id`),
  KEY `fk_barang_sub_kategori_users1_idx` (`users_id`),
  CONSTRAINT `fk_barang_sub_kategori_barang_kategori1` FOREIGN KEY (`barang_kategori_id`) REFERENCES `barang_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_barang_sub_kategori_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_sub_kategori`
--

LOCK TABLES `barang_sub_kategori` WRITE;
/*!40000 ALTER TABLE `barang_sub_kategori` DISABLE KEYS */;
INSERT INTO `barang_sub_kategori` VALUES (1,'Carseat','subkategori/gmpqRk83kM34sBdjrx88p2suSRz4kLdTwTLU9aa0.png','2018-03-22 08:30:38','2018-04-08 05:53:23',NULL,3,1),(2,'Sepatu','subkategori/qRfsx8OqXrM3ROw9jNL8YV1zxAv1sPumyPl11xJJ.png','2018-03-22 08:30:38','2018-04-08 05:53:30',NULL,5,1),(3,'Koper','subkategori/O7YsDKM3KNL3FYvHX6rzNMjLw1zvcscYg7NeX9Wk.png','2018-04-08 05:53:14','2018-04-08 05:53:14',NULL,5,1),(4,'Audio','subkategori/r60AhebxtJgTF2Rpddkz8Vbj6KlpEVWY4iEGAvXo.jpeg','2018-05-11 03:58:31','2018-05-11 03:58:31',NULL,2,1);
/*!40000 ALTER TABLE `barang_sub_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carousel`
--

DROP TABLE IF EXISTS `carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `keterangan` text,
  `tgl_awal` datetime DEFAULT NULL,
  `tgl_akhir` datetime DEFAULT NULL,
  `foto_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_carousel_users1_idx` (`users_id`),
  CONSTRAINT `fk_carousel_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carousel`
--

LOCK TABLES `carousel` WRITE;
/*!40000 ALTER TABLE `carousel` DISABLE KEYS */;
INSERT INTO `carousel` VALUES (1,'Lebaran','buy 1 get 1','2018-05-04 05:04:53','2018-05-04 05:04:53','https://ecs7.tokopedia.net/img/attachment/2018/5/16/16723082/16723082_8613c989-88dd-4592-b8ac-0167bdc35bdd.jpg','2018-05-04 05:04:53','2018-05-04 05:04:53',NULL,1);
/*!40000 ALTER TABLE `carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `h_piutang`
--

DROP TABLE IF EXISTS `h_piutang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `h_piutang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodenota` varchar(20) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `total_bayar` decimal(14,2) DEFAULT NULL,
  `payment_method` varchar(45) DEFAULT NULL,
  `status_pembayaran` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `users_peminjam_id` int(10) unsigned NOT NULL,
  `h_sewa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_h_piutang_users1_idx` (`users_peminjam_id`),
  KEY `fk_h_piutang_h_sewa1_idx` (`h_sewa_id`),
  CONSTRAINT `fk_h_piutang_h_sewa1` FOREIGN KEY (`h_sewa_id`) REFERENCES `h_sewa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_h_piutang_users1` FOREIGN KEY (`users_peminjam_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `h_piutang`
--

LOCK TABLES `h_piutang` WRITE;
/*!40000 ALTER TABLE `h_piutang` DISABLE KEYS */;
/*!40000 ALTER TABLE `h_piutang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `h_sewa`
--

DROP TABLE IF EXISTS `h_sewa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `h_sewa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodenota` varchar(20) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `tgl_sewa_awal` datetime DEFAULT NULL,
  `tgl_sewa_akhir` datetime DEFAULT NULL,
  `lama_sewa` int(11) DEFAULT NULL,
  `harga_sewa` decimal(14,2) DEFAULT NULL,
  `deposit` decimal(14,2) DEFAULT NULL,
  `grandtotal` decimal(14,2) DEFAULT NULL,
  `sisabayar` decimal(14,2) DEFAULT NULL,
  `terbayar` decimal(14,2) DEFAULT NULL,
  `status_approve` int(11) DEFAULT NULL,
  `tgl_konfirmasi` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_batas_kembali` datetime DEFAULT NULL,
  `tgl_kembali` datetime DEFAULT NULL,
  `deposit_kembali` decimal(14,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `barang_id` int(11) NOT NULL,
  `users_penyewa_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_h_sewa_barang1_idx` (`barang_id`),
  KEY `fk_h_sewa_users1_idx` (`users_penyewa_id`),
  CONSTRAINT `fk_h_sewa_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_h_sewa_users1` FOREIGN KEY (`users_penyewa_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `h_sewa`
--

LOCK TABLES `h_sewa` WRITE;
/*!40000 ALTER TABLE `h_sewa` DISABLE KEYS */;
/*!40000 ALTER TABLE `h_sewa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_barang`
--

DROP TABLE IF EXISTS `review_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci,
  `foto_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `h_sewa_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_has_barang_users1_idx` (`users_id`),
  KEY `fk_review_barang_h_sewa1_idx` (`h_sewa_id`),
  CONSTRAINT `fk_review_barang_h_sewa1` FOREIGN KEY (`h_sewa_id`) REFERENCES `h_sewa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_barang_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_barang`
--

LOCK TABLES `review_barang` WRITE;
/*!40000 ALTER TABLE `review_barang` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_url` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `aktif` tinyint(4) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(4) DEFAULT NULL,
  `no_ktp` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_ktp` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_token_UNIQUE` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sa','sa@tabsewa.com','$2y$10$tMpQL9Nw.tcmOQAc/JZwnOf/B2mMN2LMrYCCBaU5q/LQPLfwBnbUm',NULL,NULL,'2018-02-05 02:43:39','2018-02-05 02:43:39',NULL,NULL,NULL,'KXZfenZwjY8M3UHMtuM6cjXWbZkWMHrqijvUcheWvAnCxdlbgSnzHnE3AdPh',1,NULL,NULL),(2,'','',NULL,NULL,NULL,'2018-05-03 23:35:17','2018-05-03 23:35:17',1,'google','108386692143225225661','86VJQdOODiuo66AVf5ba9PbynGnWLpYMqJJPEIPFS7GlCWTiO6loxjjyyJMx',0,NULL,NULL),(3,'','',NULL,NULL,NULL,'2018-05-04 00:26:07','2018-05-04 00:26:07',1,'google','107907473357004','iqGhukicF7Zq3hKzFvOmBOfsI8ceZkXBuhdB6zI0J5VM6N2DFRlnpmn6f14t',0,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-17 16:04:29
