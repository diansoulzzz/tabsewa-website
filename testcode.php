<?php
$users_id_to = $request->get('users_id_to');
$id = $request->get('id');
if ($id != 0) {
	$last_message = Message::find($id);
	if ($last_message) {

		$messageList = Message::with(['user_from', 'user_to'])->where('id', '<', $last_message->id)
			->where(function ($query) use ($users_id_to) {
				$query->where('users_id_from', Auth::id())->where('users_id_to', $users_id_to);
			})
			->Orwhere(function ($query) use ($users_id_to) {
				$query->where('users_id_to', Auth::id())->where('users_id_from', $users_id_to);
			})
			->orderBy('id', 'desc')->get();
		$data = $messageList->take(20);
	}
} else {
	$messageList = Message::with(['user_from', 'user_to'])->where(function ($query) use ($users_id_to) {
		$query->where('users_id_from', Auth::id())->where('users_id_to', $users_id_to);
	})
		->Orwhere(function ($query) use ($users_id_to) {
			$query->where('users_id_to', Auth::id())->where('users_id_from', $users_id_to);
		})
		->orderBy('id', 'desc')->get();
	$messageListDated = $messageList->take(20);
	$list = [];
	$last_date_loop = '';
	$isShowDate = 0;
	$messageListDated = $messageListDated->reverse();
	foreach ($messageListDated as $key => $value) {
		if ($value->created_at->toDateString() != $last_date_loop) {
			$isShowDate = 1;
		} else {
			$isShowDate = 0;
		}
		$value->is_show_date = $isShowDate;
		$list[] = $value;
		$last_date_loop = $value->created_at->toDateString();
	}
	$list = collect($list)->reverse()->values()->all();
	$data = $list;
	// return $data;
}
