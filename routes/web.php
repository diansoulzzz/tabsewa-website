<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::domain('test.'.env('APP_URL_DOMAIN', 'localhost.com'))->group(function () {
//   Route::get('/', function () {
//       return "Connected";
//   });
// });
// Route::get('/', function () {
//     return "Connected";
// });

// Route::group(['domain' => 'admin-tabsewa.'.env('APP_URL_DOMAIN', 'local.com'), 'middleware'=> ['web','revalidate']], function () {
Route::group(['middleware'=> ['web','revalidate']], function () {
  Route::get('/','Admin\Auth\AdminLoginController@redirect');
  Route::get('login', 'Admin\Auth\AdminLoginController@index')->name('login');
  Route::post('login','Admin\Auth\AdminLoginController@authenticate');

  Route::group(['middleware'=>'auth'],function(){
    Route::get('logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('dashboard', 'Admin\Dashboard\AdminDashboardController@index')->name('admin.dashboard');
    Route::group(['prefix'=>'master'],function (){
      Route::group(['prefix'=>'users'],function (){
        Route::get('input', 'Admin\Master\AdminMasterUsersController@index')->name('admin.input.users');
        Route::post('input', 'Admin\Master\AdminMasterUsersController@post');
        Route::get('list', 'Admin\Master\AdminMasterUsersController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterUsersController@dataTable')->name('admin.list.users');
        Route::get('destroy', 'Admin\Master\AdminMasterUsersController@delete')->name('admin.hapus.users');
      });
      Route::group(['prefix'=>'carousel'],function (){
        Route::get('input', 'Admin\Master\AdminMasterCarouselController@index')->name('admin.input.carousel');
        Route::post('input', 'Admin\Master\AdminMasterCarouselController@post');
        Route::get('list', 'Admin\Master\AdminMasterCarouselController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterCarouselController@dataTable')->name('admin.list.carousel');
        Route::get('destroy', 'Admin\Master\AdminMasterCarouselController@delete')->name('admin.hapus.carousel');
      });
      Route::group(['prefix'=>'kategori'],function (){
        Route::get('input', 'Admin\Master\AdminMasterKategoriBrgController@index')->name('admin.input.kategori');
        Route::post('input', 'Admin\Master\AdminMasterKategoriBrgController@post');
        Route::get('list', 'Admin\Master\AdminMasterKategoriBrgController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterKategoriBrgController@dataTable')->name('admin.list.kategori');
        Route::get('destroy', 'Admin\Master\AdminMasterKategoriBrgController@delete')->name('admin.hapus.kategori');
      });
      Route::group(['prefix'=>'subkategori'],function (){
        Route::get('input', 'Admin\Master\AdminMasterSubKategoriBrgController@index')->name('admin.input.subkategori');
        Route::post('input', 'Admin\Master\AdminMasterSubKategoriBrgController@post');
        Route::get('list', 'Admin\Master\AdminMasterSubKategoriBrgController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterSubKategoriBrgController@dataTable')->name('admin.list.subkategori');
        Route::get('destroy', 'Admin\Master\AdminMasterSubKategoriBrgController@delete')->name('admin.hapus.subkategori');
      });
    });
    Route::group(['prefix'=>'transaksi'],function (){
      Route::group(['prefix'=>'dana'],function (){
        Route::get('input', 'Admin\Transaksi\AdminTransaksiDanaController@index')->name('admin.input.dana');
        Route::get('input', 'Admin\Transaksi\AdminTransaksiDanaController@otomatis')->name('admin.otomatis.dana');
        Route::post('input', 'Admin\Transaksi\AdminTransaksiDanaController@post');
        Route::get('list', 'Admin\Transaksi\AdminTransaksiDanaController@indexList');
        Route::options('list', 'Admin\Transaksi\AdminTransaksiDanaController@dataTable')->name('admin.list.dana');
        Route::get('destroy', 'Admin\Transaksi\AdminTransaksiDanaController@delete')->name('admin.hapus.dana');
      });
    });
  });
});
