<?php

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Message;
use App\Models\ReviewBarang;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function(){
  // return Message::with(['user_to','user_from'])->get();
  return ReviewBarang::with(['foto_reviews'])->get();
});

Route::prefix('search')->group(function () {
  Route::get('barang', 'Api\Search\ApiSearch@getBarang');
  Route::get('vendor', 'Api\Search\ApiSearch@getVendor');
});
Route::get('test','Firebase\FirebaseController@test');
Route::get('t','Firebase\FirebaseController@index');
Route::prefix('register')->group(function () {
  Route::post('/', 'Api\Auth\ApiRegisterController@register');
  Route::post('otp', 'Api\Auth\ApiRegisterController@registerOTP');
});
Route::prefix('login')->group(function () {
// Route::post('google', 'Api\Auth\ApiLoginController@loginSocialGoogle');
// Route::post('facebook', 'Api\Auth\ApiLoginController@loginSocialFacebook');
// Route::post('google', 'Api\Auth\ApiLoginController@loginSocialGoogle');
  Route::post('firebase', 'Api\Auth\ApiLoginController@authFirebase');
  // Route::post('server', 'Api\Auth\ApiLoginController@loginServer');
});

Route::prefix('home')->group(function () {
  Route::get('data', 'Api\Home\ApiHome@getData');
});
Route::prefix('barang-kategori')->group(function () {
  Route::get('data', 'Api\Master\ApiBarangKategori@getFromId');
  Route::get('list', 'Api\Master\ApiBarangKategori@getList');
});
Route::prefix('barang-sub-kategori')->group(function () {
  Route::get('data', 'Api\Master\ApiBarangSubKategori@getFromId');
  Route::get('list', 'Api\Master\ApiBarangSubKategori@getList');
});
Route::prefix('barang')->group(function () {
  Route::get('data/fromid', 'Api\Master\ApiBarang@getFromId');
});
Route::prefix('bank')->group(function () {
  Route::get('list', 'Api\Master\ApiMaster@getBankList');
});
Route::prefix('area')->group(function () {
  Route::get('kecamatan/list', 'Api\Master\ApiArea@getKecamatanList');
  Route::get('kota/list', 'Api\Master\ApiArea@getKotaList');
  Route::get('provinsi/list', 'Api\Master\ApiArea@getProvinsiList');
});
Route::prefix('item')->group(function () {
  Route::post('input', 'Api\Home\ApiHome@postData');
  Route::get('data', 'Api\Home\ApiHome@getItemDetail');
  Route::get('list', 'Api\Home\ApiHome@getItemList');
  Route::get('latest', 'Api\Home\ApiHome@getItemLatest');
});
Route::post('charge', 'Midtrans\MidtransController@postCharge');
Route::prefix('mt')->group(function(){
  Route::post('notification/handling', 'Midtrans\MidtransController@postNotificationHandling');
  Route::post('payment/finish', 'Midtrans\MidtransController@postPaymentFinish');
  Route::post('payment/unfinish', 'Midtrans\MidtransController@postPaymentUnfinish');
  Route::post('payment/error', 'Midtrans\MidtransController@postPaymentError');
});
Route::prefix('transaction')->group(function (){
  Route::post('rent', 'Api\Transaction\ApiTransaction@postRent');
});
Route::middleware('auth:api')->group(function () {
  // Route::prefix('m')->group(function (){
  //   Route::prefix('barang')->group(function () {
  //     Route::get('data/fromid', 'Api\Master\ApiBarang@getFromIdM');
  //   });
  // });
  // message
  Route::prefix('message')->group(function (){
    Route::post('entry','Api\Message\ApiMessage@postEntry');
    Route::get('room','Api\Message\ApiMessage@getMessageRoom');
    Route::get('take10','Api\Message\ApiMessage@get10MessageFromId');
  });
  // firebase
  Route::prefix('firebase')->group(function () {
    Route::post('fcm-token', 'Firebase\FirebaseController@postFcmToken');
  });
  // barang
  Route::prefix('barang')->group(function(){
    Route::post('entry','Api\Master\ApiBarang@postEntry');
    Route::get('is-wishlist/fromid','Api\Master\ApiBarang@getWishlistFromId');
    Route::post('toogle-wishlist/fromid','Api\Master\ApiBarang@postToogleWishlistFromId');
    Route::get('wishlist/list', 'Api\Master\ApiBarang@getWishlistList');

    // Route::get('is-lastseen-barang/fromid','Api\Master\ApiBarang@getLastSeenBarangFromId');
    Route::post('toogle-lastseen/fromid','Api\Master\ApiBarang@postToogleLastSeenBarangFromId');
    Route::get('lastseen/list', 'Api\Master\ApiBarang@getLastSeenBarangList');
  });
  // profile
  Route::prefix('profile')->group(function () {
    Route::get('info', 'Api\Profile\ApiProfile@getInfo');
    Route::post('verification', 'Api\Profile\ApiProfile@postVerification');
    Route::prefix('saldo')->group(function(){
      Route::post('withdraw', 'Api\Profile\ApiProfile@postSaldoWithdraw');
    });
  });
  // vendor
  Route::prefix('vendor')->group(function () {
    Route::post('open-toko', 'Api\Vendor\ApiVendor@postOpenToko');
    Route::prefix('transaction')->group(function(){
      Route::get('data/fromid', 'Api\Vendor\ApiVendor@getTransactionDataFromId');
      Route::get('list', 'Api\Vendor\ApiVendor@getTransactionList');
      Route::post('change', 'Api\Vendor\ApiVendor@postTransactionChange');
    });
    Route::prefix('barang')->group(function(){
      Route::post('entry','Api\Vendor\ApiVendor@postBarangEntry');
      Route::get('list', 'Api\Vendor\ApiVendor@getDaftarBarangList');
      Route::post('delete', 'Api\Vendor\ApiVendor@postBarangDelete');
    });
    Route::prefix('report')->group(function (){
      Route::get('transaksi/list/fromfilter', 'Api\Vendor\ApiVendor@getReportTransaksiListFromFilter');
    });
    Route::prefix('review')->group(function(){
      Route::get('list', 'Api\Member\ApiMember@getReviewBarangList');
    });
  });
  // member
  Route::prefix('member')->group(function () {
    Route::prefix('transaction')->group(function(){
      Route::get('data/fromid', 'Api\Member\ApiMember@getTransactionDataFromId');
      Route::get('list', 'Api\Member\ApiMember@getTransactionList');
      Route::post('change', 'Api\Member\ApiMember@postTransactionChange');
    });
    Route::prefix('review')->group(function(){
      Route::get('data/fromid', 'Api\Member\ApiMember@getReviewBarangDataFromId');
      Route::post('entry', 'Api\Member\ApiMember@postReviewBarang');
      Route::get('list', 'Api\Member\ApiMember@getReviewBarangList');
    });
  });
  // Route::prefix('transaction')->group(function (){
  //   Route::post('rent', 'Api\Transaction\ApiTransaction@postRent');
  // });
});
